import http from "../http-common";
import authHeader from './auth-header';

class ProductService {
    fetchUserById(id) {
        debugger
        return http.get(`/dashboard/getUserById/${id}`,{ headers: authHeader()});
    }

    fetchApprovedAndPendingData(){
        return http.get(`/dashboard/getApprovedAndPendingData/`,{ headers: authHeader()});
    }

    fetchUserData(id){
        return http.get(`/dashboard/getUserData/${id}`,{ headers: authHeader()});  
    }
}

export default new ProductService();