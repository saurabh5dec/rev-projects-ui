import axios from 'axios';
const apiUrl =  `http://localhost:2020`;
class SupplierService {

    fetchUser(id){  
        return axios.get(apiUrl+`/api/supplier/getUser/${id}`)    
    }

    upload(id,selectValue,amount,fromDate,paymentMode,supplierType,beneficiaryName,invoiceNumber,approvedBy,description,formData){

        return axios.post(apiUrl+`/api/supplier/upload/${id}?selectValue=`+selectValue+'&amount=' + amount +
         '&fromDate=' + fromDate + '&paymentMode=' + paymentMode +'&supplierType=' + supplierType +
          '&beneficiaryName=' + beneficiaryName +'&invoiceNumber=' + invoiceNumber +'&approvedBy=' + approvedBy
          +'&description=' + description 
            ,formData); 
   
    }

    findAllApplyData(id){
        return axios.get(apiUrl+`/api/supplier/findAllApplyData/${id}`)    
    }

    addVendor(vendor){
        return axios.post(apiUrl+`/api/supplier/addVendor`,vendor)  
    }

    findAllVendor(){
        return axios.get(apiUrl+`/api/supplier/getAllVendor`)  
    }

    deleteVendor(id){
        return axios.delete(apiUrl+`/api/supplier/deleteById/${id}`) 
    }
    UpdateVendor(vendor){
        debugger
        return axios.put(apiUrl+`/api/supplier/updateVendor`,vendor) 
    }
     updateStatus(supplierForm){
        return axios.put(apiUrl+`/api/supplier/updateStatus`,supplierForm);   
    }

    findData(fromDate,toDate,refNo,paymentStatus,employeeId){
        return axios.get(apiUrl+`/api/supplier/list/searchData?paymentStatus=`+paymentStatus+'&refNo=' + refNo +
        '&fromDate=' + fromDate + '&toDate=' + toDate+'&employeeId=' + employeeId
           ); 
 
    }

   
}
export default new SupplierService();  