
import axios from 'axios';
const apiUrl =  `http://localhost:8094`;
class ExpenseService {
    
    fetchEmployeeDetails(id){
        return axios.get(apiUrl+`/api/employee/getUserById/${id}`) 
    }

    fetchUser(id){
        return axios.get(apiUrl+`/api/employee/getUser/${id}`)    
    }
    updateBankDetail(id,BankDetail){
        return axios.put(apiUrl+`/api/employee/updateBankDetails/${id}`,BankDetail); 
    }
    upload(id,selectValue,amount,fromDate,toDate,formData){

        return axios.post(apiUrl+`/api/employee/upload/${id}?selectValue=`+selectValue+'&amount=' + amount +
         '&fromDate=' + fromDate + '&toDate=' + toDate
            ,formData); 
   
    }
    findAllApplyData(id){
        return axios.get(apiUrl+`/api/employee/findAllApplyData/${id}`)    
    }
    findData(fromDate,toDate,uniqueCode,status,name){
        return axios.get(apiUrl+`/api/employee/list/searchData?status=`+status+'&uniqueCode=' + uniqueCode +
        '&fromDate=' + fromDate + '&toDate=' + toDate+'&name=' + name
           ); 
 
    }


    findAllApplyPendingData(id){
        return axios.get(apiUrl+`/api/employee/findAllApplyPendingData/${id}`) 
    }

    updateStatus(RequestForm){
        return axios.put(apiUrl+`/api/employee/updateStatus`,RequestForm);   
    }
    updateFinanceStatus(RequestForm){
        return axios.put(apiUrl+`/api/employee/updateFinanceStatus`,RequestForm);   
    }

    applyRequest(countList) {
        debugger
        return axios.post(apiUrl+`/api/employee/apply`,countList);
    }







}
export default new ExpenseService();