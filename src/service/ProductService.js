
import http from "../http-common";
import authHeader from './auth-header';

class ProductService {

    fetchProduct() {
        return http.get("/dashboard/getProduct",{ headers: authHeader()});
    }

    addProduct(Product) {
        debugger
        return http.post('/product/addProduct', Product,{ headers: authHeader()});
    }


    fetchApplyProductList(id){
        return http.get(`/user/getAllApplyData/${id}`,{ headers: authHeader()}); 
    }

    fetchAllPendingRequest(){
        return http.get('/product/getAllPendingRequest',{ headers: authHeader()}); 
    }

    deleteCourse(id){
        return http.delete(`/product/deleteById/${id}`,{ headers: authHeader()});
    }

    updateStatus(id,selectValue){
        return http.put(`/user/updateStatusById/${id}?selectValue=`+ selectValue,null,{ headers: authHeader()})
    }

    fetchQuantity(){
        return http.get(`/user/getAllQuantity`,{ headers: authHeader()}) 
    }


    applyRequest(countList,selectValue) {
        return http.post(`/user/addQuantity?selectValue=`+ selectValue,countList,{ headers: authHeader()});
    }

    editProduct(id,Product){
        return http.put(`/product/editProduct/${id}`,Product,{ headers: authHeader()}); 
    }
    getRequest(countList){
        debugger
        return http.post(`/dashboard/getRequestToPopUp/`,countList,{ headers: authHeader()});
    }

    // downloadExcel(){
    //     return http.get(`/excel/download`,{ headers: authHeader()});
    // }


}

export default new ProductService();
