
import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
// core components
import AdminNavbar from "components/Navbars/AdminNavbar.js";
import AdminFooter from "components/Footers/AdminFooter.js";
import Sidebar from "components/Sidebar/Sidebar.js";

import routes from "routes.js";

// reactstrap components
import {
  Container,
  UncontrolledCollapse

} from "reactstrap";
class Admin extends React.Component {
  state = {
    collapsetoggle:false
  };
  toggleBox = () => {
    this.setState(prevState => ({ collapsetoggle: !prevState.collapsetoggle }));
  };
  componentDidUpdate(e) {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    this.refs.mainContent.scrollTop = 0;
  }
  getRoutes = routes => {
    return routes.map((prop, key) => {
      if (prop.layout === "/admin") {
        return (
          <Route
            path={prop.layout + prop.path}
            component={prop.component}
            key={key}
          />
        );
      } else {
        return null;
      }
    });
  };
  getBrandText = path => {
    for (let i = 0; i < routes.length; i++) {
      if (
        this.props.location.pathname.indexOf(
          routes[i].layout + routes[i].path
        ) !== -1
      ) {
        return routes[i].name;
      }
    }
    return "Brand";
  };
  render() {
    const { collapsetoggle } = this.state;
    return (
      <>
      
      <UncontrolledCollapse toggler="#toggler" className={`sidebar-slide ${collapsetoggle ? "s-out" : "s-in"}`}>
        
          <Sidebar
            {...this.props}
            routes={routes}
            logo={{
              innerLink: "/admin/main",
              imgSrc: require("assets/img/brand/fcm-logo.svg"),
              imgAlt: "..."
            }}
          />
     </UncontrolledCollapse>
        <div className={`main-content ${collapsetoggle ? "full-w" : "side-w"}`} ref="mainContent">
          <button className="navbar-toggler d-lg-inline d-xs-none d-sm-none d-md-inline"   onClick={this.toggleBox}  type="button"  id="toggler">
            <span className="navbar-toggler-icon" />
          </button>
          <AdminNavbar
            {...this.props}
            brandText={this.getBrandText(this.props.location.pathname)}
            
          />
          <Switch>
            {this.getRoutes(routes)}
            <Redirect from="*" to="/admin/main" />
          </Switch>
          
          <Container fluid>
            
            <AdminFooter />
          </Container>
        </div>
      </>
    );
  }
}

export default Admin;
