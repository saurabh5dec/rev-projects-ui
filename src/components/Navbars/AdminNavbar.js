
import React from "react";
import { Link } from "react-router-dom";
// reactstrap components
import {
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Navbar,
  Nav,
  Container,
  Media,
  Modal,
  ModalBody,
  ModalHeader,
  Col,
  Row,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupText,
  InputGroupAddon,
  Button,
  

} from "reactstrap";


class AdminNavbar extends React.Component {
  state = {
    isOpen: false
  };
  
  toggleModal = () => {
    this.setState({ isOpen: !this.state.isOpen });
  };


  render() {
    return (
      <>
        <Navbar className="navbar-top navbar-dark p-0" expand="md" id="navbar-main">
          <Container fluid className="bg-gradient-info py-1 navbar-con">
            <div className="pl-0 sm-pl-2">
              <img className="product-logo"
                alt="..."
                src={require("assets/img/brand/stationery-logo.png")}
              />
               <Link
                className="h4 ml-4 mb-0 text-white text-uppercase"
                to="/"
              >
                {this.props.brandText}
              </Link>
            </div>
            
            <Nav className="align-items-center d-none d-md-flex text-right  flex-row-reverse" navbar>
              <UncontrolledDropdown nav>
                <DropdownToggle className="pr-0" nav>
                  <Media className="align-items-center">
                    <span className="avatar avatar-sm rounded-circle">
                      <img
                        alt="..."
                        src={require("assets/img/brand/user-m.png")}
                      />
                    </span>
                    <Media className="ml-2 d-none d-lg-block">
                      <span className="mb-0 text-sm font-weight-bold">
                        Saurabh Misra
                      </span>
                    </Media>
                  </Media>
                </DropdownToggle>
                <DropdownMenu className="dropdown-menu-arrow" right>
                  <DropdownItem className="noti-title" header tag="div">
                    <h6 className="text-overflow m-0">Welcome!</h6>
                  </DropdownItem>
                  <DropdownItem to="/admin/user-profile" tag={Link}>
                    <i className="ni ni-single-02" />
                    <span>My profile</span>
                  </DropdownItem>
                  <DropdownItem onClick={this.toggleModal} tag={Link}>
                    <i className="ni ni-key-25" />
                    <span>Change Password</span>
                  </DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem href="/auth/login">
                    <i className="ni ni-user-run" />
                    <span>Logout</span>
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </Nav>
          </Container>
        </Navbar>
        {/* Change-Password modalbox */}
        <Modal isOpen={this.state.isOpen}>
          <ModalHeader toggle={this.toggleModal}>
            <h3 className="mb-0">
              Change Passward
            </h3>
          </ModalHeader>
          <ModalBody className="">
            <Form>
              <Row>
                <Col className="" lg="6">
                  <FormGroup className="mb-3">
                  <label className="form-control-label" > Passward </label>
                    <InputGroup className="input-group-alternative">
                      
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText className="bg-light-gray">
                          <i className="ni ni-key-25" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input className="bg-light-gray" placeholder="password" type="Password" autoComplete=""/>
                    </InputGroup>
                  </FormGroup>
                </Col>
                <Col className="" lg="6">
                  <FormGroup>
                  <label className="form-control-label" >Confirm Passward </label>  
                    <InputGroup className="input-group-alternative">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText className="bg-light-gray">
                          <i className="ni ni-key-25" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input className="bg-light-gray" placeholder="Confirm Password" type="password" autoComplete=""/>
                    </InputGroup>
                  </FormGroup>
                </Col>
              </Row>
            </Form>
            <div className="text-center mt-2">
              <Button className="bg-red border-0" type="button">
                Submit
              </Button>
            </div>
          </ModalBody>
        </Modal>
      </>
    );
  }
}

export default AdminNavbar;
