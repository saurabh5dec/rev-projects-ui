
/*eslint-disable*/
import React from "react";
import { NavLink as NavLinkRRD, Link } from "react-router-dom";
// nodejs library to set properties for components
import { PropTypes } from "prop-types";

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Collapse,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Media,
  NavbarBrand,
  Navbar,
  NavItem,
  NavLink,
  Nav,
  Progress,
  Table,
  Container,
  Row,
  Col
  
} from "reactstrap";
import { NavLink as RRNavLink } from 'react-router-dom';

var ps;

class Sidebar extends React.Component {
  state = {
    collapseOpen: false
  };
  constructor(props) {
    super(props);
    this.activeRoute.bind(this);
  }
  // verifies if routeName is the one active (in browser input)
  activeRoute(routeName) {
    return this.props.location.pathname.indexOf(routeName) > -1 ? "active" : "";
  }
  // toggles collapse between opened and closed (true/false)
  toggleCollapse = () => {
    this.setState({
      collapseOpen: !this.state.collapseOpen
    });
  };
  // closes the collapse
  closeCollapse = () => {
    this.setState({
      collapseOpen: false
    });
  };
  // creates the links that appear in the left menu / Sidebar
  createLinks = routes => {
    return routes.map((prop, key) => {
      return (
        <NavItem key={key}>
          <NavLink
            to={prop.layout + prop.path}
            tag={NavLinkRRD}
            onClick={this.closeCollapse}
            activeClassName="active"
          >
            <i className={prop.icon} />
            {prop.name}
           
          </NavLink>
        </NavItem>
      );
    });
  };
  render() {
    const { bgColor, routes, logo } = this.props;
    let navbarBrandProps;
    if (logo && logo.innerLink) {
      navbarBrandProps = {
        to: logo.innerLink,
        tag: Link
      };
    } else if (logo && logo.outterLink) {
      navbarBrandProps = {
        href: logo.outterLink,
        target: "_blank"
      };
    }

    return (
      
        <Navbar
          className=" navbar-vertical navbar-dark bg-primary dark-shadow"
          expand="md"
          id="sidenav-main"
        >
          <div className="overlay d-lg-inline d-md-inline d-sm-none d-xs-none"></div>
          <Container fluid>
            {/* Toggler */}
            <button
              className="navbar-toggler"
              type="button"
              onClick={this.toggleCollapse}
            >
              <span className="navbar-toggler-icon text-white" />
            </button>
            {/* Brand */}
            {logo ? (
              <NavbarBrand className="p-2 bg-white side-logo" {...navbarBrandProps}>
                <img
                  alt={logo.imgAlt}
                  className="navbar-brand-img"
                  src={logo.imgSrc}
                />
              </NavbarBrand>
            ) : null}
            {/* User */}
            <Nav className="align-items-center d-md-none">
              {/* <UncontrolledDropdown nav>
                <DropdownToggle nav className="nav-link-icon">
                  <i className="ni ni-bell-55" />
                </DropdownToggle>
                <DropdownMenu
                  aria-labelledby="navbar-default_dropdown_1"
                  className="dropdown-menu-arrow"
                  right
                >
                  <DropdownItem>Action</DropdownItem>
                  <DropdownItem>Another action</DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem>Something else here</DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown> */}
              <UncontrolledDropdown nav>
                <DropdownToggle nav>
                  <Media className="align-items-center">
                    <span className="avatar avatar-sm rounded-circle">
                      <img
                        alt="..."
                        src={require("assets/img/brand/user-m.png")}
                      />
                    </span>
                  </Media>
                </DropdownToggle>
                <DropdownMenu className="dropdown-menu-arrow" right>
                    <DropdownItem className="noti-title" header tag="div">
                      <h6 className="text-overflow m-0">Welcome!</h6>
                    </DropdownItem>
                    <DropdownItem to="/admin/user-profile" tag={Link}>
                      <i className="ni ni-single-02" />
                      <span>My profile</span>
                    </DropdownItem>
                    <DropdownItem to="/admin/user-profile" tag={Link}>
                      <i className="ni ni-key-25" />
                      <span>Change Password</span>
                    </DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem href="/auth/login">
                      <i className="ni ni-user-run" />
                      <span>Logout</span>
                    </DropdownItem>
                  </DropdownMenu>
              </UncontrolledDropdown>
            </Nav>
            {/* Collapse */}
            <Collapse className="fcm-menu-main" navbar isOpen={this.state.collapseOpen}>
              {/* Collapse header */}
              <div className="navbar-collapse-header d-md-none">
                <Row>
                  {logo ? (
                    <Col className="collapse-brand" xs="6">
                      {logo.innerLink ? (
                        <Link to={logo.innerLink}>
                          <img alt={logo.imgAlt} src={logo.imgSrc} />
                        </Link>
                      ) : (
                        <a href={logo.outterLink}>
                          <img alt={logo.imgAlt} src={logo.imgSrc} />
                        </a>
                      )}
                    </Col>
                  ) : null}
                  <Col className="collapse-close" xs="6">
                    <button
                      className="navbar-toggler"
                      type="button"
                      onClick={this.toggleCollapse}
                    >
                      <span />
                      <span />
                    </button>
                  </Col>
                </Row>
              </div>
              {/* Form */}
              <Form className="mt-4 mb-3 d-md-none">
                <InputGroup className="input-group-rounded input-group-merge">
                  <Input
                    aria-label="Search"
                    className="form-control-rounded form-control-prepended"
                    placeholder="Search"
                    type="search"
                  />
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <span className="fa fa-search" />
                    </InputGroupText>
                  </InputGroupAddon>
                </InputGroup>
              </Form>
              {/* Stationery Menu */}
                <Nav className="fcm-menu-nav navbar-nav">
                  <UncontrolledDropdown nav activeClassName="active">
                    <DropdownToggle nav className="nav-link-icon">
                      <i className="ni ni-ruler-pencil" />
                      Stationery
                    </DropdownToggle>
                    <DropdownMenu 
                      aria-labelledby="navbar-default_dropdown_1"
                      className="dropdown-menu-arrow"
                      right
                    >
                      <DropdownItem to="/admin/main" tag={Link}>
                        <i className="ni ni-tv-2 text-red" />
                        <span>Dashboard</span>
                      </DropdownItem>
                      <DropdownItem to="/admin/report" tag={Link}>
                        <i className="ni ni-collection text-red" />
                        <span>Report</span>
                      </DropdownItem>
                      <DropdownItem to="/admin/addproduct" tag={Link}>
                        <i className="ni ni-fat-add text-red" />
                        <span>Stationery Add Product</span>
                      </DropdownItem>
                      <DropdownItem to="/admin/adminreport" tag={Link}>
                        <i className="ni ni-ui-04 text-red" />
                        <span>Product Status Report</span>
                      </DropdownItem>
                    </DropdownMenu>
                  </UncontrolledDropdown>
                {/* Expence Tool Menu */}
                  <UncontrolledDropdown nav>
                    <DropdownToggle nav className="nav-link-icon">
                      <i className="ni ni-money-coins text-orange" />
                      Expense Claim
                    </DropdownToggle>
                    <DropdownMenu
                      aria-labelledby="navbar-default_dropdown_1"
                      className="dropdown-menu-arrow"
                      right
                    >
                       {/* Reimbursement Module Menu */}
                      <DropdownItem to="/admin/reimbursement-form"  tag={Link}>
                        <i className="ni ni-paper-diploma text-red" />
                        <span>Reimbursement Form</span>
                      </DropdownItem>
                      <DropdownItem to="/admin/payment-traker-report"  tag={Link}>
                        <i className="ni ni-chart-pie-35 text-red" />
                        <span>Payment Tracker Report</span>
                      </DropdownItem>
                      <DropdownItem to="/admin/approver"  tag={Link}>
                        <i className="ni ni-check-bold text-red" />
                        <span>Reimbursement Approver</span>
                      </DropdownItem>

                       {/* Incentive Module Menu */}
                      <DropdownItem to="/admin/incentive"  tag={Link}>
                        <i className="ni ni-money-coins text-red" />
                        <span>Incentive Report</span>
                      </DropdownItem>

                       {/* Imprest Module Menu */}
                      <DropdownItem to="/admin/imprest"  tag={Link}>
                        <i className="ni ni-single-copy-04 text-red" />
                        <span>Imprest Report</span>
                      </DropdownItem>

                       {/* Supplier Module Menu */}
                      <DropdownItem to="/admin/supplier-form"  tag={Link}>
                        <i className="ni ni-delivery-fast text-red" />
                        <span>Supplier Form</span>
                      </DropdownItem>
                      <DropdownItem to="/admin/supplier-report"  tag={Link}>
                        <i className="ni ni-single-copy-04 text-red" />
                        <span>Supplier Report</span>
                      </DropdownItem>
                      <DropdownItem to="/admin/vendor-form"  tag={Link}>
                        <i className="ni ni-single-copy-04 text-red" />
                        <span>Add Vendor</span>
                      </DropdownItem>
                      <DropdownItem to="/admin/vendor-report"  tag={Link}>
                        <i className="ni ni-single-copy-04 text-red" />
                        <span>Vendor Report</span>
                      </DropdownItem>
                    </DropdownMenu>
                  </UncontrolledDropdown>
                {/* other Menu */}
                  <UncontrolledDropdown nav>
                    <DropdownToggle nav className="nav-link-icon">
                      <i className="ni ni-email-83" />
                      Fcm SMS
                    </DropdownToggle>
                    <DropdownMenu
                      aria-labelledby="navbar-default_dropdown_1"
                      className="dropdown-menu-arrow"
                      right
                    >
                      <DropdownItem to="#" tag={Link}>
                        <i className="ni ni-tv-2 text-red" />
                        <span>Dashboard</span>
                      </DropdownItem>
                    </DropdownMenu>
                  </UncontrolledDropdown>
                </Nav>
              {/* Navigation */}
              {/* <Nav navbar>{this.createLinks(routes)}</Nav> */}
            </Collapse>
          </Container>
        </Navbar>
    );
  }
}

Sidebar.defaultProps = {
  routes: [{}]
};

Sidebar.propTypes = {
  // links that will be displayed inside the component
  routes: PropTypes.arrayOf(PropTypes.object),
  logo: PropTypes.shape({
    // innerLink is for links that will direct the user within the app
    // it will be rendered as <Link to="...">...</Link> tag
    innerLink: PropTypes.string,
    // outterLink is for links that will direct the user outside the app
    // it will be rendered as simple <a href="...">...</a> tag
    outterLink: PropTypes.string,
    // the image src of the logo
    imgSrc: PropTypes.string.isRequired,
    // the alt for the img
    imgAlt: PropTypes.string.isRequired
  })
};

export default Sidebar;

