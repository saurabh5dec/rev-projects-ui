
import React from "react";

// reactstrap components
import {
  Badge,
  Card,
  CardHeader,
  CardFooter,
  Pagination,
  PaginationItem,
  PaginationLink,
  Table,
  Container,
  Row,
  Button,
} from "reactstrap";
import ReactPaginate from 'react-paginate';
// core components

import StationeryHearderCard from "views/stationery/StationeryHeaderCard.jsx";
import ProductService from "service/ProductService";
class Productreport extends React.Component {

  constructor(props) {
    super(props);
 
    this.state = {
      orgtableData: [],
      offset: 0,
      perPage: 10,
      currentPage: 0,
      tableData:[]
    }
     this.handlePageClick =this.handlePageClick.bind(this);
     this.reloadApplyProductList=this.reloadApplyProductList.bind(this)
  }

  componentDidMount() {
    this.reloadApplyProductList();
}

reloadApplyProductList() {

  const user = JSON.parse(localStorage.getItem('user'));
  ProductService.fetchApplyProductList(user.id)
      .then((res) => {
        var data = res.data;
        var slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)
          this.setState({
            orgtableData:res.data,
            pageCount: Math.ceil(data.length / this.state.perPage),
            tableData:slice,
          })
      });

}



handlePageClick = (e) =>{
  const selectedPage = e.selected;
  const offset= selectedPage * this.state.perPage;
  this.setState({
    currentPage:selectedPage,
    offset:offset
  }, () =>{
    this.loadMoreData();
   });
}


loadMoreData(){
  const data=this.state.orgtableData;
  const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)
  this.setState({
    pageCount: Math.ceil(data.length / this.state.perPage),
    tableData:slice,
  });
}



  render() {
    return (
      <>
        <StationeryHearderCard />
        {/* Page content */}
        <Container className="mt--7" fluid>
          {/* Table */}
          <Row>
            <div className="col">
              <Card className="shadow">
                <CardHeader className="border-0">
                  <h3 className="mb-0 d-inline-block float-left">Report</h3>
                  <a href="http://localhost:9090/excel/download">
                  <Button  className="bg-red d-inline-block float-right">Download Excel <i className="fas fa-download" aria-hidden="true"></i></Button>
                  </a>
                </CardHeader>
                <Table className="align-items-center table-flush" responsive>
                  <thead className="thead-light">
                    <tr>
                      <th scope="col">ID</th>
                      <th scope="col">Date</th>
                      <th scope="col">Name</th>
                      <th scope="col">Branch</th>
                      <th scope="col">Product Name</th>
                      <th scope="col">Quantity</th>
                      <th scope="col">Status</th>
                    </tr>
                  </thead>
                  <tbody>

                  { this.state.tableData.map(rproduct => 
                    <tr key={rproduct.id}>
                      <td>{rproduct.id}</td>
                      <td>{rproduct.createdOn}</td>
                      <td>{rproduct.name}</td>
                      <td>{rproduct.branch}</td>
                      <td>{rproduct.productName}</td>
                      <td>{rproduct.quantity}</td>
                      <td>
                        <Badge color="" className="badge-dot mr-4">
                          <i className="bg-warning" />
                          {rproduct.status}
                        </Badge>
                      </td>
                    </tr>
                  )}
                  </tbody>
                </Table>
                <CardFooter className="py-4">
                  <nav aria-label="...">
                    {/* <Pagination
                      className="pagination justify-content-end mb-0"
                      listClassName="justify-content-end mb-0"
                    >
                      <PaginationItem className="disabled">
                        <PaginationLink
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                          tabIndex="-1"
                        >
                          <i className="fas fa-angle-left" />
                          <span className="sr-only">Previous</span>
                        </PaginationLink>
                      </PaginationItem>
                      <PaginationItem className="active">
                        <PaginationLink
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          1
                        </PaginationLink>
                      </PaginationItem>
                      <PaginationItem>
                        <PaginationLink
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          2 <span className="sr-only">(current)</span>
                        </PaginationLink>
                      </PaginationItem>
                      <PaginationItem>
                        <PaginationLink
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          3
                        </PaginationLink>
                      </PaginationItem>
                      <PaginationItem>
                        <PaginationLink
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          <i className="fas fa-angle-right" />
                          <span className="sr-only">Next</span>
                        </PaginationLink>
                      </PaginationItem>
                    </Pagination> */}
                     <ReactPaginate
                    previousLabel={"prev"}
                    nextLabel={"next"}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={this.state.pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={this.handlePageClick}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"}/>
                  </nav>
                </CardFooter>
              </Card>
            </div>
          </Row>
         </Container>
      </>
    );
  }
}

export default Productreport;
