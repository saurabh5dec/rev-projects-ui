
import React from "react";

// reactstrap components
import {
  Card,
  CardHeader,
  CardFooter,
  Pagination,
  PaginationItem,
  PaginationLink,
  Table,
  Container,
  Row,
  Button,
  CardBody,
  CardTitle,
  Col,FormGroup,Input
} from "reactstrap";
import ReactPaginate from 'react-paginate';
import ProductService from 'service/ProductService'
import UserService from "service/UserService";
import swal from 'sweetalert';

class Adminreport extends React.Component {

  constructor(props) {
    super(props);
 
    this.state = {

      orgtableData: [],
      offset: 0,
      perPage: 10,
      currentPage: 0,
      pendingRequest: [],
      selectValue:"Pending",
      userData:[],
      approvedAndPendingData:[]
    }
    this.handleDropdownChange = this.handleDropdownChange.bind(this);
    this.handlePageClick =this.handlePageClick.bind(this);
  }

  componentDidMount() {
    this.reloadAllPendingRequest();
    this.getDataFormUserLoginId();
    this.getApprovedAndPendingData();
}


//this function is used to show all pending record 
reloadAllPendingRequest() {
    ProductService.fetchAllPendingRequest()
        .then((res) => {
          var data = res.data;
          var slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)
            this.setState({
              orgtableData:res.data,
              pageCount: Math.ceil(data.length / this.state.perPage),
              pendingRequest:slice,
            })
        });
        console.log(this.state.pendingRequest)
}

//this function is used to handle pagination logic 
handlePageClick = (e) =>{
  debugger
  const selectedPage = e.selected;
  const offset= selectedPage * this.state.perPage;
  this.setState({
    currentPage:selectedPage,
    offset:offset
  }, () =>{
    this.loadMoreData();
   });
}


loadMoreData(){
   debugger
  const data=this.state.orgtableData;
  const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)
  this.setState({
    pageCount: Math.ceil(data.length / this.state.perPage),
    pendingRequest:slice,
  });
}



//this function to used set the dropdown value  from state
handleDropdownChange(e) {
  this.setState({ selectValue: e.target.value });
}


//this fuction is update the status of product
updateStatusClicked(id) {
 // let selectValue = {selectValue: this.state.selectValue,};
  ProductService.updateStatus(id, this.state.selectValue)
      .then(
          response => {
            swal({
              title:"Success",
              text:"Apply  Product Updated sucessfully",
              icon:"success",
              timer:3000,
              buttons:false,
            })
              window.location.reload();
              this.reloadAllPendingRequest()
          }
      ).catch(err =>{
        swal({
          title:"Success",
          text:"Product Updated sucessfully Email not trigger",
          icon:"success",
          timer:3000,
          buttons:false,
        })
        window.location.reload();
      }
      
        )

}

//this fuction is used to show the in header section
getDataFormUserLoginId(){
  const user = JSON.parse(localStorage.getItem('user'));
  UserService.fetchUserById(user.id) 
  .then((res) => {
    
    this.setState({
      userData:res.data
    });
  });
}

//this function is used to get last approved data and total pending products
getApprovedAndPendingData(){
 
  UserService.fetchApprovedAndPendingData() 
  .then((res) => {
    this.setState({
      approvedAndPendingData:res.data
    });
  });
}


  render() {
    return (
      <>
        <div className="header pb-8 pt-3 pt-md-6">
          <Container fluid>
            <div className="header-body">
              {/* Card stats */}
              <Row>
                <Col lg="4" xl="4">
                  <Card className="card-stats mb-4 mb-xl-0">
                    <CardBody>
                      <Row>
                        <div className="col">
                          <CardTitle
                            tag="h5"
                            className="text-uppercase text-muted mb-0"
                          >
                            Last Approved Order
                          </CardTitle>
                          <span className="h5 font-weight-bold mb-0">
                          {this.state.approvedAndPendingData.productName} <br></br> <small>Quantity : {this.state.approvedAndPendingData.quantity}</small>
                          </span>
                        </div>
                        <Col className="col-auto">
                          <div className="icon icon-shape bg-warning text-white rounded-circle shadow">
                            <i className="fas fa-shopping-cart" />
                          </div>
                        </Col>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
                <Col lg="4" xl="4">
                  <Card className="card-stats mb-4 mb-xl-0">
                    <CardBody>
                      <Row>
                        <div className="col">
                          <CardTitle
                            tag="h5"
                            className="text-uppercase text-muted mb-0"
                          >
                            Pending Order
                          </CardTitle>
                          <span className="h5 font-weight-bold mb-0">
                            {this.state.approvedAndPendingData.pendingProduct} Product <br></br>
                            <small> status : pending</small>
                          </span>
                        </div>
                        <Col className="col-auto">
                          <div className="icon icon-shape bg-yellow text-white rounded-circle shadow">
                            <i className="fas fa-shopping-basket" />
                          </div>
                        </Col>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
                <Col lg="4" xl="4">
                  <Card className="card-stats mb-4 mb-xl-0">
                    <CardBody>
                      <Row>
                        <div className="col">
                          <CardTitle
                            tag="h5"
                            className="text-uppercase text-muted mb-0"
                          >
                            Employee Details
                          </CardTitle>
                          <span className="h5 font-weight-bold mb-0">
                            Admin Branch : <small>{this.state.userData.costCenter}</small>
                          </span>
                          <br></br>
                          <span className="h5 font-weight-bold mb-0">
                           Employee ID : <small>{this.state.userData.employeeId}</small>
                          </span>
                        </div>
                        <Col className="col-auto">
                          <div className="icon icon-shape bg-info text-white rounded-circle shadow">
                            <i className="fas fa-user" />
                          </div>
                        </Col>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </div>
          </Container>
        </div> 
        {/* Page content */}
        <Container className="mt--7" fluid>
          {/* Table */}
          <Row>
            <div className="col">
              <Card className="shadow">
                <CardHeader className="border-0">
                  <h3 className="mb-0 d-inline-block float-left">Report</h3>
                  <a href="http://localhost:9090/excel/download/pending">
                  <Button className="bg-red d-inline-block float-right">Download Excel <i className="fas fa-download" aria-hidden="true"></i></Button>
                 </a>
                </CardHeader>
                <Table className="align-items-center table-flush" responsive>
                  <thead className="thead-light">
                    <tr>
                      <th scope="col">ID</th>
                      <th scope="col">Date</th>
                      <th scope="col">Name</th>
                      <th scope="col">Branch</th>
                      <th scope="col">Cost Cenetr</th>
                      <th scope="col">Product Name</th>
                      <th scope="col">Quantity</th>
                      <th scope="col" className="table-status-w">Status</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  { this.state.pendingRequest.map(pendingReq => 
                    <tr key={pendingReq.id}>
                      <td>{pendingReq.id}</td>
                       <td>{pendingReq.createdOn}</td>
                        <td>{pendingReq.name}</td>
                         <td>{pendingReq.branch}</td>
                        <td>{pendingReq.costCenter}</td>
                        <td>{pendingReq.productName}</td>
                        <td>{pendingReq.price}</td>
                      <td>
                        {/* <select id="dropdown" onChange={this.handleDropdownChange} >
            
                       <option value="Pending">Pending</option>
                      <option value="Rejected">Rejected</option>
                        <option value="Cancel">Cancel</option>
                          <option value="Done">Done</option>
                       </select> */}
                        <FormGroup className="mb-0">
                          <Input type="select" name="select" id="dropdown" onChange={this.handleDropdownChange}>
                            <option value="Pending">Pending</option>
                            <option value="Rejected">Rejected</option>
                            <option value="Cancel">Cancel</option>
                            <option value="Done">Done</option>
                          </Input>
                        </FormGroup>
                      </td>
                      <td>
                        <Button className="text-success p-0 btn-bg" disabled>
                          <i className="fas fa-save text-16" onClick={() => this.updateStatusClicked(pendingReq.id)}/>
                        </Button>
                      </td>
                    </tr>
                  )}
                    {/* <tr>
                      <td>2</td>
                      <td>20 Aug 2020</td>
                      <td>Saurabh Misra</td>
                      <td>Noida-BO</td>
                      <td>6701-NOI-IT</td>
                      <td>NoteBook</td>
                      <td>3</td>
                      <td>
                        <FormGroup className="mb-0">
                          <Input type="select" name="select" id="">
                            <option selected>Pending</option>
                            <option>Rejected</option>
                            <option>Cancel</option>
                            <option>Done</option>
                          </Input>
                        </FormGroup>
                      </td>
                      <td>
                        <Button className="text-success p-0 btn-bg" disabled>
                          <i className="fas fa-save text-16" />
                        </Button>
                      </td>
                    </tr>
                     */}
                  </tbody>
                </Table>
                <CardFooter className="py-4">
                  <nav aria-label="...">
                    {/* <Pagination
                      className="pagination justify-content-end mb-0"
                      listClassName="justify-content-end mb-0"
                    >
                      <PaginationItem className="disabled">
                        <PaginationLink
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                          tabIndex="-1"
                        >
                          <i className="fas fa-angle-left" />
                          <span className="sr-only">Previous</span>
                        </PaginationLink>
                      </PaginationItem>
                      <PaginationItem className="active">
                        <PaginationLink
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          1
                        </PaginationLink>
                      </PaginationItem>
                      <PaginationItem>
                        <PaginationLink
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          2 <span className="sr-only">(current)</span>
                        </PaginationLink>
                      </PaginationItem>
                      <PaginationItem>
                        <PaginationLink
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          3
                        </PaginationLink>
                      </PaginationItem>
                      <PaginationItem>
                        <PaginationLink
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          <i className="fas fa-angle-right" />
                          <span className="sr-only">Next</span>
                        </PaginationLink>
                      </PaginationItem>
                    </Pagination> */}
                     <ReactPaginate
                    previousLabel={"prev"}
                    nextLabel={"next"}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={this.state.pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={this.handlePageClick}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"}/>
                  </nav>
                </CardFooter>
              </Card>
            </div>
          </Row>
         </Container>
      </>
    );
  }
}

export default Adminreport;
