
import React from "react";
// reactstrap components
import {
  Button,
  Card,
  CardBody, 
  CardHeader,
  Table,
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  InputGroup,Modal,
  InputGroupModal,
  ModalHeader,
  ModalBody
} from "reactstrap";
import ReactPaginate from 'react-paginate';
import ProductService from "service/ProductService";
import swal from 'sweetalert';

class AddProduct extends React.Component {

  constructor(props) {
		super(props);
    this.state = {
			  productName:null,
        unitPrice: null,
        products:[],
        productname:"",
        price:"",
        isOpen: false,
        id:0,
        orgtableData: [],
        offset: 0,
        perPage: 5,
        currentPage: 0,
        search:"",
      }
     
     this.reloadProductList = this.reloadProductList.bind(this);
  }


  componentDidMount() {
    this.reloadProductList();
}
// this method to call list of Products
reloadProductList() {
    ProductService.fetchProduct()
        .then((res) => {
          var data = res.data;
          var slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)
         // const products = res.data;
            this.setState({
              products:slice,
              orgtableData:res.data,
              pageCount: Math.ceil(data.length / this.state.perPage),
            })
        });

        console.log(this.state.products)
}

//this function used to set  input value of product price
handlePrice = event => {
  this.setState({ unitPrice: event.target.value,
  });
}
//this function used to set input of product name
handleProductname = event => {
  this.setState({ productName: event.target.value,
  });
}

//this function used to save a new product
  saveProduct = (e) => {
    e.preventDefault();
    let Product = {productName: this.state.productName, unitPrice: this.state.unitPrice,};
    ProductService.addProduct(Product)
        .then(res => {
          swal({
            title:"Success",
            text:"Product Added sucessfully",
            icon:"success",
            timer:2000,
            buttons:false,
          })
          
           this.reloadProductList()
           window.location.reload();
        });
}
  
//this function is used to delete the product from product List
deleteCourseClicked(id) {

  ProductService.deleteCourse(id)
      .then(
          response => {
            swal({
              title:"Success",
              text:"Product Deleted sucessfully",
              icon:"success",
              timer:2000,
              buttons:false,
            })
            
              this.reloadProductList()
          }
      )

}


//this function to used open a popup
toggleModal = (product) => {
    this.setState({ isOpen: !this.state.isOpen,
      id:product.id,
      productname:product.productName,
      price:product.unitPrice,
    });
};


//this function to used edit a product
editData = (e) =>{
  debugger
  let Product = {productName: this.state.productName, unitPrice: this.state.unitPrice,};
  alert(JSON.stringify(Product));
  ProductService.editProduct(this.state.id,Product)
  .then(
    response => {
        this.setState({ message: `Edit of Product  Successful` })
        this.reloadProductList();
        window.location.reload();
    }
)
}

//this function is used tohandle the pagination logic
handlePageClick = (e) =>{
  debugger
  const selectedPage = e.selected;
  const offset= selectedPage * this.state.perPage;
  this.setState({
    currentPage:selectedPage,
    offset:offset
  }, () =>{
    this.loadMoreData();
   });
}


loadMoreData(){
   debugger
  const data=this.state.orgtableData;
  const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)
  this.setState({
    pageCount: Math.ceil(data.length / this.state.perPage),
    products:slice,
  });
}



//this function is used to filter the data based on productName
onChange = (e) =>{
this.setState({
  search:e.target.value,
  
}, () => {
  this.globalSearch();
});
}


globalSearch = () => {

  let { search, orgtableData } = this.state;
  let products = orgtableData.filter(value => {
  return (
      value.productName.toLowerCase().includes(search.toLowerCase())
    );
  });
  this.setState({ products });
};










 render() {
    return (
      <>
      <div className="header pb-8 pt-3 pt-md-6">
          <Container fluid>
            <div className="header-body">              {/* Card stats */}
              <Row>
                  <Col lg="12" xl="12">
                    <Card className="card-stats mb-4 mb-xl-0">
                      <CardBody>
                        <Row>
                          <div className="col">
                             <h3 className="mb-0">Add Product</h3>
                            <span className="h5 font-weight-bold mb-0">
                              <small className="text-muted">Name should be unique</small>
                            </span>
                            <Form role="form" className="mt-3">
                              <Row>
                                <Col className="" lg="6">
                                  <FormGroup className="mb-3">
                                    <InputGroup className="input-group-alternative">
                                      <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                          <i className="ni ni-ruler-pencil" />
                                        </InputGroupText>
                                      </InputGroupAddon>
                                      <Input placeholder="Product Name" value={this.state.productName} type="text" onChange={this.handleProductname} autoComplete=""/>
                                    </InputGroup>
                                  </FormGroup>
                                </Col>
                                <Col className="" lg="6">
                                  <FormGroup>
                                    <InputGroup className="input-group-alternative">
                                      <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                          <i className="ni ni-tag" />
                                        </InputGroupText>
                                      </InputGroupAddon>
                                      <Input placeholder="Price Per Unit" value={this.state.unitPrice} onChange={this.handlePrice} type="text" autoComplete=""/>
                                    </InputGroup>
                                  </FormGroup>
                                </Col>
                              </Row>
                              <div className="text-center mt-3">
                                <Button className="bg-light-gray border-0" type="button" onClick={this.saveProduct}>
                                  Submit
                                </Button>
                              </div>
                            </Form>
                          </div>
                          {/* <Col className="col-auto">
                            <div className="icon icon-shape bg-warning text-white rounded-circle shadow">
                              <i className="fas fa-plus" />
                            </div>
                          </Col> */}
                        </Row>
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
              </div>
           </Container>
        </div>
        {/* Page content */}
        <Container className="mt--7" fluid>
          <Row className="">
            <Col className="mb-5 mb-xl-0" xl="12">
              <Card className="shadow">
                <CardHeader className="border-0">
                  <Row className="align-items-center">
                    <div className="col">
                      <h3 className="mb-0">Product List</h3>
                    </div>
                    <div className="col text-right">
                      <Form className="float-right navbar-search navbar-search-light form-inline mr-3 d-none d-md-flex ml-lg-auto">
                        <FormGroup className="mb-0">
                          <InputGroup className="input-group-alternative">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="fas fa-search" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input placeholder="Search" onChange={this.onChange} type="text" />
                          </InputGroup>
                        </FormGroup>
                      </Form>
                    </div>
                  </Row>
                </CardHeader>
                
                <Table className="align-items-center table-flush" responsive>
                  <thead className="thead-light">
                    <tr>
                      <th scope="col">ID</th>
                      <th scope="col">Added On</th>
                      <th scope="col">Product Name</th>
                      <th scope="col">Price (INR)</th>
                      <th scope="col">Added By</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  { this.state.products.map(product => 
                    <tr key={product.id}>
                      <td>{product.id}</td>
                      
                     <td>{product.createdOn}</td>
                     {/* {this.renderCells()} */}
                       <td>{product.productName}</td>
                        <td>{product.unitPrice}</td> 
                        <td>{product.updatedBy}</td>
                      <td>
                      <Form id='table-editaction' method='' action='#'>
                      <Button className="text-success p-0" onClick={()=>this.toggleModal(product)}>
                              <i className="fas fa-edit text-16" />
                            </Button>
                             <Button className="text-danger p-0">
                               <i className="fas fa-trash text-16" onClick={() => this.deleteCourseClicked(product.id)}/>
                            </Button>
                        </Form>
                      </td>
                    </tr>
                  )}
                    {/* <tr>
                      <td>2</td>
                      <td>20-Aug-2020</td>
                      {this.renderCells()}
                      <td>Saurabh Misra</td>
                      <td>
                      <Form id='table-editaction' method='' action='#'>
                            <Button className="text-success p-0" disabled>
                              <i className="fas fa-save text-16" />
                            </Button>
                             <Button className="text-danger p-0">
                               <i className="fas fa-trash text-16" />
                            </Button>
                        </Form>
                      </td>
                    </tr> */}
                  </tbody>
                </Table>
                <nav aria-label="...">
                  <ReactPaginate
                  previousLabel={"prev"}
                    nextLabel={"next"}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={this.state.pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={this.handlePageClick}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"}/>
                  </nav>
                <div className="text-center mt-3 mb-3">
                  <Button className="btn-gray border-0" type="button" onClick={() => this.props.history.push('/admin/report')}>
                    Submit
                  </Button>
                </div>
              </Card>
            </Col>
          </Row>
{/* Edit modalbox */}
       <Modal className="modal-size"  isOpen={this.state.isOpen}>
            <ModalHeader className="bg-light-gray" toggle={this.toggleModal}>
              <h3 className="mb-0 text-16">
                Please Confirm Your Request
              </h3>
            </ModalHeader>
            <ModalBody className="pt-0">
              <Form role="form" className="mt-3">
                <Row>
                  <Col className="" lg="6">
                    <FormGroup className="mb-3">
                      <InputGroup className="input-group-alternative">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="ni ni-ruler-pencil" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input placeholder={this.state.productname} onChange={this.handleProductname} type="text" autoComplete=""/>
                      </InputGroup>
                    </FormGroup>
                  </Col>
                  <Col className="" lg="6">
                    <FormGroup>
                      <InputGroup className="input-group-alternative">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="ni ni-tag" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input placeholder={this.state.price} onChange={this.handlePrice} type="text" autoComplete=""/>
                      </InputGroup>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col className="text-center mt-4 pt-1" lg="12">
                    <Button className="bg-light-gray border-0" onClick={this.editData} type="button">
                      Confirm
                    </Button>
                    <Button className="bg-light-gray border-0" type="button">
                      Cancel
                    </Button>
                  </Col>
                </Row> 
              </Form>
            </ModalBody>
          </Modal>


        </Container>
      </>
    );
  }
  
// renderCells() {
    
//   return ['productname', 'price'].map(field => (
//     <Cell key={field} value={ this.state[field] } onChange={value => this.setState({[field]: value})} />
//   ))
  
// };

componentDidUpdate() {
  const { productname, price } = this.state;
  console.log(`New State: ${productname} - ${price}`);
};


}


class Cell extends React.Component {

constructor(props) {
  super(props);
  this.state = { editing: false };
}

  render() {

  const { value, onChange } = this.props;

  return this.state.editing ?
    <td className="no-pad"><input className="form-control editor edit-text" ref='input' 
    value={value} onChange={e => onChange(e.target.value)} onBlur={ e => this.onBlur()} /></td> :
    <td onClick={() => this.onFocus()}>{value}</td>
}

onFocus() {
  this.setState({ editing: true }, () => this.refs.input.focus());
}

onBlur() {
  this.setState({ editing: false });
}
}
export default AddProduct;

