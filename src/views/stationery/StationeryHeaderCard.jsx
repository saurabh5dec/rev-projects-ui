
import React from "react";

// reactstrap components
import { Card, CardBody, CardTitle, Container, Row, Col } from "reactstrap";
import UserService from "../../service/UserService"

class StationeryHeaderCard extends React.Component {


  constructor(props) {
    super(props);
 
    this.state = {
      userData:[],
      requestData:[],
    }
   
  }

  componentDidMount() {
    this.getDataFormUserLoginId();
    this.getUserData();
}

//this funtion is used to filed the user data on header section
  getDataFormUserLoginId(){
    const user = JSON.parse(localStorage.getItem('user'));
    UserService.fetchUserById(user.id) 
    .then((res) => {
      this.setState({
        userData:res.data
      });
    });
  }


getUserData(){
  const user = JSON.parse(localStorage.getItem('user'));
    UserService.fetchUserData(user.id) 
    .then((res) => {
      this.setState({
        requestData:res.data
      });
    });
}



  render() {
    return (
      <>
      <div className="header pb-8 pt-3 pt-md-6">
          <Container fluid>
            <div className="header-body">
              {/* Card stats */}
              <Row>
                <Col lg="4" xl="4">
                  <Card className="card-stats mb-4 mb-xl-0">
                    <CardBody>
                      <Row>
                        <div className="col">
                          <CardTitle
                            tag="h5"
                            className="text-uppercase text-muted mb-0"
                          >
                            Last Order
                          </CardTitle>
                          <span className="h5 font-weight-bold mb-0">
                          {this.state.userData.productName} <br></br> <small>Quantity : {this.state.userData.quantity}</small>
                          </span>
                        </div>
                        <Col className="col-auto">
                          <div className="icon icon-shape bg-warning text-white rounded-circle shadow">
                            <i className="fas fa-shopping-cart" />
                          </div>
                        </Col>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
                <Col lg="4" xl="4">
                  <Card className="card-stats mb-4 mb-xl-0">
                    <CardBody>
                      <Row>
                        <div className="col">
                          <CardTitle
                            tag="h5"
                            className="text-uppercase text-muted mb-0"
                          >
                            All Order
                          </CardTitle>
                          <span className="h5 font-weight-bold mb-0">
                            Products <br></br>
                            <small>Quanity : {this.state.userData.totalQuantity}</small>
                          </span>
                        </div>
                        <Col className="col-auto">
                          <div className="icon icon-shape bg-yellow text-white rounded-circle shadow">
                            <i className="fas fa-shopping-basket" />
                          </div>
                        </Col>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
                <Col lg="4" xl="4">
                  <Card className="card-stats mb-4 mb-xl-0">
                    <CardBody>
                      <Row>
                        <div className="col">
                          <CardTitle
                            tag="h5"
                            className="text-uppercase text-muted mb-0"
                          >
                            Employee Details
                          </CardTitle>
                          <span className="h5 font-weight-bold mb-0">
                            Cost Center : <small>{this.state.requestData.costCenter}</small>
                          </span>
                          <br></br>
                          <span className="h5 font-weight-bold mb-0">
                           Employee ID : <small>{this.state.requestData.userCode}</small>
                          </span>
                        </div>
                        <Col className="col-auto">
                          <div className="icon icon-shape bg-info text-white rounded-circle shadow">
                            <i className="fas fa-user" />
                          </div>
                        </Col>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </div>
          </Container>
        </div> 
      </>
    );
  }
}

export default StationeryHeaderCard;
