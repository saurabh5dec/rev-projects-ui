
import React from "react";

// reactstrap components
import { Button,CardFooter, Card,CardHeader,Table,Container,Row,Col,Form,FormGroup,InputGroupAddon, InputGroupText, Input,
  InputGroupInputGroup,Modal,
  InputGroup,
  ModalHeader,
  ModalBody} from "reactstrap";
  import ReactPaginate from 'react-paginate';
import StationeryHearderCard from "views/stationery/StationeryHeaderCard.jsx";
import ProductService from "../../service/ProductService";
import swal from 'sweetalert';




class StationeryMain extends React.Component {

  constructor(props) {
    super(props);
 
    this.state = {
      products: [],
      count:[],
      countList:[],
      isOpen: false,
      data:[],
      selectedProduct:[],
      selectValue:"Gurgaon",
      orgtableData: [],
      offset: 0,
      perPage: 5,
      currentPage: 0,
      search:"",
  
    }
    this.handleDropdownChange = this.handleDropdownChange.bind(this);
      this.updateInputsArrayIncrement = this.updateInputsArrayIncrement.bind(this)
      this.updateInputsArrayDecrement = this.updateInputsArrayDecrement.bind(this);
      this.reloadProductList = this.reloadProductList.bind(this)
      this.handlePageClick =this.handlePageClick.bind(this);
  }

  componentDidMount() {
    this.reloadProductList();
}

//this method is used to get all product from database 
reloadProductList() {
    ProductService.fetchProduct()
        .then((res) => {
          const products = res.data;
          var quantity=[];
          for (const [index, value] of products.entries()) {
            quantity.push(value.quantity)
          }
          var data = res.data;
          var slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)
            this.setState({
              products:slice,
              pageCount: Math.ceil(data.length / this.state.perPage),
              countList:quantity,
              orgtableData:res.data,
            })
        });
}

//this method is used to handle the pagination logic
handlePageClick = (e) =>{
  const selectedPage = e.selected;
  const offset= selectedPage * this.state.perPage;
  this.setState({
    currentPage:selectedPage,
    offset:offset
  }, () =>{
    this.loadMoreData();
   });
}


loadMoreData(){
   debugger
  const data=this.state.orgtableData;
  const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)
  this.setState({
    pageCount: Math.ceil(data.length / this.state.perPage),
    products:slice,
  });
}

//this function is used to decrement the quantity
 updateInputsArrayDecrement(newValue,index) {
  //copy the array first
 const updatedArray = [...this.state.countList];
        newValue=newValue-1
 updatedArray[index] = newValue;
 this.setState({
      countList: updatedArray,
  });
}


//this function is used to increment the quantity
updateInputsArrayIncrement(newValue,index) {
  //copy the array first
 const updatedArray = [...this.state.countList];
 if(newValue==""){
    newValue=newValue+1
     updatedArray[index] = newValue;
    this.setState({
        countList: updatedArray,
     });
   }else{
      var number = parseInt(newValue, 10)  + 1;
    updatedArray[index] = number;
     this.setState({
      countList: updatedArray,
   });
 }
}


//this function to used to apply the product
applyProduct = () => {
  const user = JSON.parse(localStorage.getItem('user'));
  const credentials = {countList:this.state.countList,userId:user.id}
  ProductService.applyRequest(credentials,this.state.selectValue)
  .then((res) => {
    swal({
      title:"Success",
      text:"Product apply sucessfully",
      icon:"success",
      timer:2000,
      buttons:false,
    })
    this.props.history.push('/admin/report')
    this.reloadProductList();
  });
}

//this funtion is used to open a popup
toggleModal = () => {
  const credentials = {countList:this.state.countList}
  ProductService.getRequest(credentials)
  .then((res) => {
    const selectedProduct = res.data;
    this.setState({selectedProduct
  
    });
  });
   this.setState({ isOpen: !this.state.isOpen,
    countList:this.state.countList,

  });
};

//this function is used to select the location from dropdown 
handleDropdownChange(e) {
  this.setState({ selectValue: e.target.value });
}

//this fuction is used to filter the product based on productName
onChange = (e) =>{
this.setState({
  search:e.target.value,
  
}, () => {
  this.globalSearch();
});
}


globalSearch = () => {
  
  let { search, orgtableData } = this.state;
  let products = orgtableData.filter(value => {
  return (
      value.productName.toLowerCase().includes(search.toLowerCase())
    );
  });
  this.setState({ products });
};




  render() {
  
    return (
      <>
        <StationeryHearderCard/>
        {/* Page content */}
        <Container className="mt--7" fluid>
          <Row className="">
            <Col className="mb-5 mb-xl-0" xl="12">
              <Card className="shadow">
                <CardHeader className="border-0">
                  <Row className="align-items-center">
                    <div className="col">
                      <h3 className="mb-0">Product List</h3>
                    </div>
                    <div className="col text-right">
                      <Form className="float-right navbar-search navbar-search-light form-inline mr-3 d-none d-md-flex ml-lg-auto">
                        <FormGroup className="mb-0">
                          <InputGroup className="input-group-alternative">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="fas fa-search" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input placeholder="Search" onChange={this.onChange} type="text" />
                          </InputGroup>
                        </FormGroup>
                      </Form>
                    </div>
                  </Row>
                </CardHeader>
                <Table className="align-items-center table-flush" responsive>
                  <thead className="thead-light">
                    <tr>
                      <th scope="col">Product ID</th>
                      <th scope="col">Product Name</th>
                      <th scope="col">Product Type</th>
                      <th scope="col">Product Price (INR)</th>
                      <th scope="col">Product Quantity</th>
                    </tr>
                  </thead>
                  <tbody>      
                 { this.state.products.map((product,index) => 
                    <tr key={product.id}>
                                        <td>{product.id}</td>
                                        <td>{product.productName}</td>
                                        <td>{product.categoryType}</td>
                                        <td>{product.unitPrice}</td>
                                      
                      <td>
                        <Form id='productquantity' method='' action='#'>
                            <Button className="qtymin p-0">
                              <i  id={this.state.countList[index]} onClick={e => this.updateInputsArrayDecrement(e.target.id,index)} className="ni ni-fat-delete text-16" />
                            </Button>
                             <input type='text'  value={this.state.countList[index]}   className='qty' />
                             <Button className="qtypls p-0">
                               <i className="ni ni-fat-add text-16" id={this.state.countList[index]} onClick={e => this.updateInputsArrayIncrement(e.target.id,index)}  />
                            </Button>
                        </Form>
                      </td>
                    </tr>
        )}


                    
                    {/* <tr>
                      <th scope="row">102</th>
                      <td>Pen</td>
                      <td>Stationery</td>
                      <td>10</td>
                      <td>
                        <Form id='productquantity' method='' action='#'>
                            <Button className="qtymin p-0">
                              <i className="ni ni-fat-delete text-16" />
                            </Button>
                             <input type='text' name='quantity' value="0" className='qty' />
                             <Button className="qtypls p-0">
                               <i className="ni ni-fat-add text-16" />
                            </Button>
                        </Form>
                      </td>
                    </tr>
                    */}
                  </tbody>
                </Table>
                  <nav aria-label="...">
                  <ReactPaginate
                  previousLabel={"prev"}
                    nextLabel={"next"}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={this.state.pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={this.handlePageClick}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"}/>
                  </nav>
                <div className="text-center mt-3 mb-3">
                  <Button className="bg-light-gray border-0" type="button"  onClick={()=>this.toggleModal()}>
                    Submit
                  </Button>
                </div>
              </Card>
            </Col>
          </Row>

          {/* confirmation modalbox */}
          <Modal className="modal-size"  isOpen={this.state.isOpen}>
                  <ModalHeader toggle={this.toggleModal}>
                    <h3 className="mb-0 text-16">
                      Please Confirm Your Request
                    </h3>
                  </ModalHeader>
                  <ModalBody className="pt-0">
                    <Table className="align-items-center table-flush" responsive>
                    <thead className="thead-light">
                      <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Product Name</th>
                        <th scope="col">Price (INR)</th>
                        <th scope="col">Quantity</th>
                      </tr>
                    </thead>
                    <tbody>
                    { this.state.selectedProduct.map((selectproduct,index) => 
                    <tr key={selectproduct.id}>
                       <td>{selectproduct.id}</td>
                       <td>{selectproduct.productName}</td>
                        <td>{selectproduct.unitPrice}</td>
                        <td>{selectproduct.quantity}</td>
                      </tr>
                    )}
                    </tbody>
  
                  </Table>
                   <Row>
                      <Col lg="6">
                        <h5 className="">Select Product Delivery Location</h5>
                        <FormGroup className="mt-1">
                          <Input type="select" name="select" onChange={this.handleDropdownChange} id="dropdown">
                            <option value="Gurgaon">Gurgaon</option>
                            <option value="Delhi">Delhi</option>
                            <option value="Noida">Noida</option>
                            <option value="Bangloar">Bangloar</option>
                          </Input>
                        </FormGroup>
                      </Col>
                      <Col className="text-center mt-4 pt-1" lg="6">
                        <Button className="bg-light-gray border-0" onClick={this.applyProduct} type="button">
                          Confirm
                        </Button>
                        <Button className="bg-light-gray border-0" type="button">
                          Cancel
                        </Button>
                      </Col>
                   </Row> 
                  </ModalBody>
                </Modal>
        </Container>
      </>
    );
  }
}

export default StationeryMain;
