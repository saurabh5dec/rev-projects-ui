
import React from "react";

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  Container,
  Row,
  Col
} from "reactstrap";
// core components
// import UserHeader from "components/Headers/UserHeader.js";

class UserProfile extends React.Component {
  render() {
    return (
      <>
        {/* <UserHeader /> */}
        <div
          className="header pb-2 pt-5 pt-lg-5 d-flex align-items-center"
        >
          {/* Header container */}
          <Container className="d-flex align-items-center" fluid>
            <Row>
              <Col className="mb-5 mb-xl-0 mt-5 " xl="12" lg="4">
                <Card className="card-profile shadow bg-gradient-default">
                  <Row className="justify-content-center">
                    <Col className="order-lg-2" lg="3">
                      <div className="card-profile-image">
                        <a href="#pablo" onClick={e => e.preventDefault()}>
                          <img
                            alt="..."
                            className="rounded-circle"
                            src={require("assets/img/brand/user-m.png")}
                          />
                        </a>
                      </div>
                    </Col>
                  </Row>
                  <CardBody className="mt-8 pt-md-4">
                    
                    <div className="text-center">
                      <h3 className="text-white">
                        Saurabh Mishra
                        <span className="font-weight-light">, FCM/IND/3222</span>
                      </h3>
                      <div className="h5 font-weight-300 text-white">
                        <i className="ni location_pin mr-2" />
                        Branch : Technology Noida-HO
                      </div>
                      <div className="h5 mt-4 text-white">
                        <i className="ni business_briefcase-24 mr-2" />
                        Assistant Manager - FCM Technology
                      </div>
                    </div>
                  </CardBody>
                </Card>
              </Col>
            </Row>
              <Col className="ml-3" xl="8" lg="8">
                <Card className="bg-secondary shadow">
                  <CardHeader className="bg-light-gray border-0">
                    <Row className="align-items-center">
                      <Col xs="8">
                        <h3 className="mb-0">User information</h3>
                      </Col>
                    </Row>
                  </CardHeader>
                  <CardBody className="">
                    <Form>
                      <div className="pl-lg-4">
                        <Row>
                          <Col lg="6">
                            <FormGroup>
                              <label
                                className="form-control-label"
                                htmlFor="input-username"
                              >
                                Username
                              </label>
                              <Input
                                className="form-control-alternative"
                                defaultValue="Saurabh"
                                id="input-username"
                                placeholder="Username"
                                type="text"
                              />
                            </FormGroup>
                          </Col>
                          <Col lg="6">
                            <FormGroup>
                              <label
                                className="form-control-label"
                                htmlFor="input-email"
                              >
                                Email address
                              </label>
                              <Input
                                className="form-control-alternative"
                                id="input-email"
                                placeholder="saurabh.misra@in.fcm.travel"
                                type="email"
                              />
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Col lg="6">
                            <FormGroup>
                              <label
                                className="form-control-label"
                                htmlFor="input-first-name"
                              >
                                First name
                              </label>
                              <Input
                                className="form-control-alternative"
                                defaultValue="Saurabh"
                                id="input-first-name"
                                placeholder="First name"
                                type="text"
                              />
                            </FormGroup>
                          </Col>
                          <Col lg="6">
                            <FormGroup>
                              <label
                                className="form-control-label"
                                htmlFor="input-last-name"
                              >
                                Last name
                              </label>
                              <Input
                                className="form-control-alternative"
                                defaultValue="Misra"
                                id="input-last-name"
                                placeholder="Last name"
                                type="text"
                              />
                            </FormGroup>
                          </Col>
                        </Row>
                      </div>
                      {/* Address */}
                      <h6 className="heading-small text-muted mb-4">
                        Contact information
                      </h6>
                      <div className="pl-lg-4">
                        <Row>
                          <Col md="12">
                            <FormGroup>
                              <label
                                className="form-control-label"
                                htmlFor="input-address"
                              >
                                Address
                              </label>
                              <Input
                                className="form-control-alternative"
                                defaultValue="888 FCM Travel Solution Udhyog Vihar"
                                id="input-address"
                                placeholder="Home Address"
                                type="text"
                              />
                            </FormGroup>
                          </Col>
                        </Row>
                      </div>
                    </Form>
                  </CardBody>
                </Card>
              </Col>
          </Container>
        </div>
       </>
    );
  }
}

export default UserProfile;
