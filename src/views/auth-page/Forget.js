
import React from "react";

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Col,
  CardFooter
} from "reactstrap";

class Forget extends React.Component {
  render() {
    return (
      <>
        <Col className="login-pg" lg="5" md="7">
          <Card className="bg-secondary shadow border-0">
            <CardHeader className="bg-transparent pb-4 pt-4">
              <div className="text-center">
                <div
                  className=""
                  color="default"
                >
                  <span className="login-logo">
                    <img
                      alt="..."
                      src={require("assets/img/brand/fcm-logo.svg")}
                    />
                  </span>
                </div>
                
              </div>
              <div className="text-center text-muted mt-2">
                <small> Sign in with credentials</small>
              </div>
            </CardHeader>
            <CardBody className="px-lg-5 pt-5">
              <Form role="form">
                <FormGroup className="mb-3">
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-email-83" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input placeholder="Email" type="email" autoComplete="new-email"/>
                  </InputGroup>
                </FormGroup>
                <div className="custom-control custom-control-alternative custom-checkbox">
                  <a
                className="float-right text-red"
                href="login"
              >
                <small>Login Here?</small>
              </a>
                </div>
                <div className="text-center mt-3">
                  <Button className="bg-red border-0" type="button"  onClick={() => this.props.history.push('/auth/login')}>
                   Submit
                  </Button>
                </div>
              </Form>
            </CardBody>
            <CardFooter className="bg-light-gray">
            <img className="w-100" alt="..." src={require("assets/img/brand/footer-logos.png")} />
            </CardFooter>
          </Card>
        </Col>
      </>
    );
  }
}

export default Forget;
