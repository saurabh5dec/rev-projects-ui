
import React from "react";

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Col,
  CardFooter
} from "reactstrap";
import AuthService from "../../service/AuthService"
import swal from 'sweetalert';

class Login extends React.Component {

  constructor(props) {
    super(props);
 
    this.state = {
     username:"",
     password:"",
    }
    
  }

  handleUserName = event => {
    this.setState({ username: event.target.value,
    });
  }

  handlePassword = event => {
    this.setState({ password: event.target.value,
    });
  }
  //this.props.history.push('/admin/main')

  login = () => {
    const loginRequest = {username:this.state.username,password:this.state.password}
    debugger
    AuthService.login(loginRequest)
    .then(response => {
      if (response.data.accessToken) {
        debugger
        localStorage.setItem("userInfo", JSON.stringify(response.data));
        this.props.history.push('/admin/main')
      }
      return response.data;
    }).catch(err =>{
      swal({
        title:"Error",
        text:"User Name or Password is wrong",
        icon:"error",
        timer:3000,
        buttons:false,
      })
    })
  }
  

  render() {
    return (
      <>
        <Col className="login-pg" lg="5" md="7">
          <Card className="bg-secondary shadow border-0">
            <CardHeader className="bg-transparent pb-4 pt-4">
              <div className="text-center">
                <div
                  className=""
                  color="default"
                >
                  <span className="login-logo">
                    <img
                      alt="..."
                      src={require("assets/img/brand/fcm-logo.svg")}
                    />
                  </span>
                </div>
                
              </div>
              <div className="text-center text-muted mt-2">
                <small> Sign in with credentials</small>
              </div>
            </CardHeader>
            <CardBody className="px-lg-5 py-lg-4 pt-0">
              <Form role="form">
                <FormGroup className="mb-3">
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-email-83" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input placeholder="Email" value={this.state.username} onChange={this.handleUserName} type="email" autoComplete="new-email"/>
                  </InputGroup>
                </FormGroup>
                <FormGroup>
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-lock-circle-open" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input placeholder="Password" type="password" value={this.state.password} onChange={this.handlePassword} autoComplete="new-password"/>
                  </InputGroup>
                </FormGroup>
                <div className="custom-control custom-control-alternative custom-checkbox">
                  <input
                    className="custom-control-input"
                    id=" customCheckLogin"
                    type="checkbox"
                  />
                  <label
                    className="custom-control-label"
                    htmlFor=" customCheckLogin"
                  >
                    <span className="">Remember me</span>
                  </label>
                  <a
                className="float-right text-red"
                href="forget">
                <small>Forgot password?</small>
              </a>
                </div>
                <div className="text-center mt-3">
                  <Button className="bg-red border-0" type="button" onClick={this.login}>
                    Sign in
                  </Button>
                </div>
              </Form>
            </CardBody>
            <CardFooter className="bg-light-gray">
            <img className="w-100" alt="..." src={require("assets/img/brand/footer-logos.png")} />
            </CardFooter>
          </Card>
        </Col>
      </>
    );
  }
}

export default Login;
