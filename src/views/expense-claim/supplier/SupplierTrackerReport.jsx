
import React from "react";

// reactstrap components
import {
  Button,
  Card,
  CardTitle,
  CardBody,
  CardHeader,
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Badge,
  Label,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  InputGroup

 
} from "reactstrap";
import moment from 'moment';
import ReactPaginate from 'react-paginate';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import SupplierService from "service/SupplierService";
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import { formatDate, parseDate } from 'react-day-picker/moment'

class SupplierTrackerReport extends React.Component {
// confirmation Popup
constructor(props) {
  super(props);
// confirmation Popup
this.state = {
  isOpen: false,
  isGas:false,
  from: undefined,
  to: undefined,
  requestFormData:[],
  userData:[],
  offset: 0,
  perPage: 10,
  currentPage: 0,
  tableData:[],
  vendorData:[],
  selectValue:'',
  rejectedStatus:'',

};
this.handleFromChange = this.handleFromChange.bind(this);
this.handleToChange = this.handleToChange.bind(this);
this.fetchUserById = this.fetchUserById.bind(this)
this.findAllVendor= this.findAllVendor.bind(this);
this.handleExpenseDropdownChange =this.handleExpenseDropdownChange.bind(this);
this.handleChangeRejectStatus=this.handleChangeRejectStatus.bind(this)
this.handleRefNo=this.handleRefNo.bind(this)
this.handleEmployeeId=this.handleEmployeeId.bind(this)
this.handlePaymentStatus=this.handlePaymentStatus.bind(this)
}

  componentDidMount() {
  this.findAllPendingRequest();
  this.fetchUserById();
  this.findAllVendor();
} 

//this function is used to select the location from dropdown 
handleExpenseDropdownChange(e){
  this.setState({ selectValue: e.target.value });
}

handleChangeRejectStatus(e){
  this.setState({ rejectedStatus: e.target.value });
}

findAllVendor(){
  SupplierService.findAllVendor()
  .then((res) => {
    this.setState({
      vendorData:res.data,
    })
});
    
}

findAllPendingRequest(){
  const user = JSON.parse(localStorage.getItem('user'));
    SupplierService.findAllApplyData(user.id)
        .then((res) => {
          var data = res.data;

          var slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)
          this.setState({
            requestFormData:res.data,
            pageCount: Math.ceil(data.length / this.state.perPage),
            tableData:slice,
          })
      });
          
}

toggleModal = (requestFormData) => {
  let url= 'http://localhost:2020/api/supplier/download/?fileName='+requestFormData.invFileName;
  let url1= 'http://localhost:2020/api/supplier/download/?fileName='+requestFormData.fileName;
  this.setState({
         isOpen: !this.state.isOpen,
         uniqueCode:requestFormData.refNo,
         amount:requestFormData.amount,
         approvedBy:requestFormData.approvedBy,
         fileName:requestFormData.fileName,
         invFileName:requestFormData.invFileName,
         api:url,
         api1:url1,
         status:requestFormData.paymentStatus
       });
     
};

toggleGasModal = (requestFormData) => {
    let url= 'http://localhost:2020/api/supplier/download/?fileName='+requestFormData.invFileName;
    let url1= 'http://localhost:2020/api/supplier/download/?fileName='+requestFormData.fileName;
  this.setState({ isGas: !this.state.isOpen ,
    uniqueCode:requestFormData.refNo,
    amount:requestFormData.amount,
    approvedBy:requestFormData.approvedBy,
    fileName:requestFormData.fileName,
    invFileName:requestFormData.invFileName,
    api:url,
    api1:url1,
    status:requestFormData.paymentStatus,
    invoiceNo:requestFormData.invoiceNo,
    invoiceDate:requestFormData.invoiceDate,
    invoiceType:requestFormData.invoiceType,
  });

};

  // constructor(props) {
  //   super(props);
  //   this.handleFromChange = this.handleFromChange.bind(this);
  //   this.handleToChange = this.handleToChange.bind(this);
  //   this.state = {
  //     from: undefined,
  //     to: undefined,
  //   };
  // }
  showFromMonth() {
    const { from, to } = this.state;
    if (!from) {
      return;
    }
    if (moment(to).diff(moment(from), 'months') < 2) {
      this.to.getDayPicker().showMonth(from);
    }
  }

  handleFromChange(from) {
    // Change the from date and focus the "to" input field
    this.setState({ from });
  }

  handleToChange(to) {
    this.setState({ to }, this.showFromMonth);
  }

  fetchUserById(){
    const user = JSON.parse(localStorage.getItem('user'));
    SupplierService.fetchUser(user.id)
        .then((res) => {
          this.setState({
            userData:res.data,
          });
        });
  }

  updateStatus =(e) =>{
  
    const supplierForm={paymentStatus:this.state.selectValue,rejectedStatus:this.state.rejectedStatus,refNo:this.state.uniqueCode}
    SupplierService.updateStatus(supplierForm)
    .then((res) => {
      this.setState({
        
        //requestFormData:res.data,
      });
    });
  }


  handleRefNo=(e)=>{
  this.setState({
    refNo:e.target.value
  })
  }

  handlePaymentStatus=(e)=>{
  this.setState({
    paymentStatus:e.target.value
  })
  }
  handleEmployeeId=(e)=>{
    this.setState({
    employeeId:e.target.value
  })
  }

  searchData =(e) =>{
    debugger
    SupplierService.findData(this.state.from,this.state.to,this.state.refNo,
      this.state.paymentStatus,this.state.employeeId)
    .then((res) => {
      this.setState({
        tableData:res.data,
      });
    });
  }







  render() {
    const { from, to } = this.state;
    const modifiers = { start: from, end: to };
    return (
      <>
      {/* Employee Detail */}
      <div className=" pb-2 pt-5 pt-md-5">
          <Container fluid>
            <div className="">
              {/* Card stats */}
              <Row>
                <Col lg="6" xl="6">
                  <Card className="card-stats mb-4 mb-xl-0">
                    <CardBody>
                      <Row>
                        <div className="col">
                          <CardTitle
                            tag="h5"
                            className="text-uppercase text-muted mb-0"
                          >
                            Approver Details
                          </CardTitle>
                          <span className="h5 font-weight-bold mb-0">
                        Approver Name: <span>{this.state.userData.approvedBy}</span>
                          </span>
                          <br></br>
                          <span className="h5 font-weight-bold mb-0">
                         Email : <span>{this.state.userData.approvedEmail}</span>
                          </span>
                        </div>
                        <Col className="col-auto">
                          <div className="icon icon-shape bg-warning text-white rounded-circle shadow">
                            <i className="fas fa-user" />
                          </div>
                        </Col>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
                <Col lg="6" xl="6">
                  <Card className="card-stats mb-4 mb-xl-0">
                    <CardBody>
                      <Row>
                        <div className="col">
                          <CardTitle
                            tag="h5"
                            className="text-uppercase text-muted mb-0"
                          >
                            Employee Details
                          </CardTitle>
                          <span className="h5 font-weight-bold mb-0">
                          Branch : <span>{this.state.userData.branch}</span>
                          </span>
                          <br></br>
                          <span className="h5 font-weight-bold mb-0">
                          Employee ID : <span>{this.state.userData.employeeId}</span>
                          </span>
                        </div>
                        <Col className="col-auto">
                          <div className="icon icon-shape bg-info text-white rounded-circle shadow">
                            <i className="fas fa-user" />
                          </div>
                        </Col>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </div>
          </Container>
        </div> 
      {/* Search Form */}
        <div className="header pb-3 pt-4 pt-md-4">
          <Container fluid>
            <div className="header-body">
              {/* Card stats */}
              <Row>
                  <Col lg="12" xl="12">
                    <Card className="card-stats mb-4 mb-xl-0">
                      <CardBody>
                        <Row>
                          <div className="col">
                             <h3 className="mb-0">Details</h3>
                            <Form role="form" className="mt-3 Custom-form">
                              <Row>
                                <Col className="Subfrom-to" lg="12">
                                  <Row>
                                    <Col className="" lg="6">
                                      <FormGroup>
                                        <Label for="FromDate">Submission Date From</Label><br></br>
                                        <DayPickerInput type="date"
                                        value={from}
                                        placeholder="From"
                                        format="LL"
                                        formatDate={formatDate}
                                        parseDate={parseDate}
                                        dayPickerProps={{
                                          selectedDays: [from, { from, to }],
                                          disabledDays: { after: to },
                                          toMonth: to,
                                          modifiers,
                                          numberOfMonths: 2,
                                          onDayClick: () => this.to.getInput().focus(),
                                        }}
                                        onDayChange={this.handleFromChange}
                                      />
                                      </FormGroup>
                                    </Col>
                                    <Col className="" lg="6">
                                      <FormGroup>
                                        <Label for="ToDate">To</Label><br></br>
                                        <DayPickerInput type="text" 
                                          ref={el => (this.to = el)}
                                          value={to}
                                          placeholder="To"
                                          format="LL"
                                          formatDate={formatDate}
                                          parseDate={parseDate}
                                          dayPickerProps={{
                                            selectedDays: [from, { from, to }],
                                            disabledDays: { before: from },
                                            modifiers,
                                            month: from,
                                            fromMonth: from,
                                            numberOfMonths: 2,
                                          }}
                                          onDayChange={this.handleToChange}
                                        />
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                </Col>
                                <Col className="" lg="4">
                                    <FormGroup>
                                      <Label for="referenceid">Payment reference ID</Label>
                                      <Input
                                        type="number"
                                        name="referenceid"
                                        placeholder="Payment reference ID"
                                        onChange={this.handleRefNo}
                                      />
                                    </FormGroup>
                                  </Col>
                                <Col className="" lg="4">
                                  <FormGroup>
                                    <Label for="PaymentStatus">Payment Status</Label>
                                    <Input type="select" name="select" onChange={this.handlePaymentStatus} id="PaymentStatus">
                                      <option> Pending </option>
                                      <option> On Hold</option>
                                      <option> In Process</option>
                                      <option> Paid</option>
                                      <option>Processed</option>
                                      <option>Rejected</option>
                                      <option>Document Received</option>

                                    </Input>
                                  </FormGroup>
                                </Col>
                                <Col className="" lg="4">
                                    <FormGroup>
                                      <Label for="Employeename">Employee ID </Label>
                                      <Input
                                        type="text"
                                        name="Employeename"
                                        placeholder="Employee ID"
                                        onChange={this.handleEmployeeId}
                                      />
                                    </FormGroup>
                                  </Col>
                              </Row>
                              <div className="text-center mt-3">
                                <Button className="bg-red border-0" onClick={this.searchData} type="button">
                                  Search
                                </Button>
                              </div>
                            </Form>
                          </div>
                        </Row>
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
              </div>
           </Container>
        </div>
      {/* Page content */}
      
        <Container className="mt-3" fluid>
          <Row className="">
            <Col className="mb-5 mb-xl-0" xl="12">
              <Card className="shadow">
                <CardHeader className="border-0 pb-0">
                  <Row className="align-items-center">
                    <div className="col">
                      <h3 className="mb-0">Reimbursement Tracking Report</h3>
                    </div>
                    <div className="text-center mt-3"> 
                                        <ReactHTMLTableToExcel  
                                                className="btn btn-info"  
                                                table="SupplierReport"  
                                                filename="ReportExcel"  
                                                sheet="Sheet"  
                                                buttonText="Export excel" />  
                      <Button className="bg-red border-0" type="button">
                        <i className="fas fa-download mr-2"></i>
                        Export Master Excel
                      </Button>
                    </div>
                  </Row>
                </CardHeader>
                <CardBody>
                  <Table id="SupplierReport" className="align-items-center table-flush" responsive>
                    <thead className="thead-light">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Sub Date</th>
                        <th scope="col">Name</th>
                        <th scope="col">Branch</th>
                        <th scope="col">Emp. ID</th>
                        <th scope="col">Amount (INR)</th>
                        <th scope="col">Approved By</th>
                        <th scope="col">Approved Attachemnt</th>
                        <th scope="col">Invoice Attachemnt</th>
                        <th scope="col">Approver Status</th>
                        <th scope="col">Print</th>
                        <th scope="col">Detail</th>
                        <th scope="col">GAS</th>
                      </tr>
                    </thead>
                    <tbody>

                    { this.state.tableData.map((requestFormData,index) => 
                      <tr key={requestFormData.id}>
                         <td>{requestFormData.id}</td>
                         <td>{requestFormData.subdate}</td>
                         <td>{requestFormData.name}</td>
                         <td>{requestFormData.branch}</td>
                         <td>{requestFormData.employeeId}</td>
                         <td>{requestFormData.amount}</td>
                         <td>{requestFormData.approvedBy}</td>
                         <td>{requestFormData.fileName}</td>
                         <td>{requestFormData.invFileName}</td>
                         <td>{requestFormData.paymentStatus}</td>
                      
                        <td><Button  type="button"  className="text-primary shadow-none p-0"><i className="fas fa-print"  aria-hidden="true"></i> Print</Button></td>
                        <td><Button  type="button" onClick={(e) => this.toggleModal(requestFormData)} className="text-success shadow-none p-0"><i class="fas fa-eye" aria-hidden="true"></i> View</Button></td>
                        <td><Button  type="button" onClick={(e) => this.toggleGasModal(requestFormData)} className="text-success shadow-none p-0"><i class="fas fa-eye" aria-hidden="true"></i> GAS</Button></td>
                      </tr>
                      )}
                    </tbody>
                  </Table>
                  <ReactPaginate
                    previousLabel={"prev"}
                    nextLabel={"next"}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={this.state.pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={this.handlePageClick}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"}/>
                </CardBody>
              </Card>
            </Col>
          </Row>
         {/* View modalbox */}
         <Modal className="modal-size"  isOpen={this.state.isOpen}>
            <ModalHeader className="bg-light-gray" toggle={this.toggleModal}>
              <h3 className="mb-0 text-16">
                Report
              </h3>
            </ModalHeader>
            <ModalBody className="pt-0">
            <Row>
              <div className="col">
                <Form role="form" className="mt-3 Custom-form">
                  <Row>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Refrence ID 
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="809828909" value={this.state.uniqueCode} type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Submission Date 
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="20-07-2020" value={this.state.subdate} type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Amount 
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="3000" value={this.state.amount} type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Approved By
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="Abha Varma" value={this.state.approvedBy} type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Approved Attachemnt
                        </Label>
                        <a href={this.state.api1} className="text-danger">{this.state.fileName} </a>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Invoice Attachemnt
                        </Label>
                        <a href={this.state.api} className="text-danger"> {this.state.invFileName} </a>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="12">
                      <FormGroup>
                        <Label>
                          Description
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="testing process" type="textarea" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Payment Statue
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="Pending" value={this.state.status} type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Recjected Reason
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="Test Data"  onChange={this.handleChangeRejectStatus} type="text" autoComplete=""/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label for="PaymentStatus">Status</Label>
                        <Input type="select"   onChange={this.handleExpenseDropdownChange} name="select" id="PaymentStatus">
                          <option> Pending </option>
                          <option> On Hold</option>
                          <option> In Process</option>
                          <option> Paid</option>
                          <option>Processed</option>
                          <option>Rejected</option>
                          <option>Document Received</option>

                        </Input>
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
              </div>
            </Row>
              <Row>
                <Col className="text-center mt-4 pt-1" lg="12">
                  <Button onClick={this.updateStatus} className="bg-red border-0" type="button">
                    Confirm
                  </Button>
                  <Button className="bg-light-gray border-0" type="button">
                    Cancel
                  </Button>
                </Col>
              </Row> 
            </ModalBody>
          </Modal>

          <Modal className="modal-size"  isOpen={this.state.isGas}>
            <ModalHeader className="bg-light-gray" toggle={this.toggleGasModal}>
              <h3 className="mb-0 text-16">
                Gas Report
              </h3>
            </ModalHeader>
            <ModalBody className="pt-0">
            <Row>
              <div className="col">
                <Form role="form" className="mt-3 Custom-form">
                  <Row>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Refrence ID 
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="809828909" value={this.state.uniqueCode} type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Submission Date 
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="20-07-2020" value={this.state.subdate} type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Amount 
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="3000" value={this.state.amount} type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Approved By
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="Abha Varma" value={this.state.approvedBy} type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Approved Attachemnt
                        </Label>
                        <a href={this.state.api1} className="text-danger">{this.state.fileName} </a>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Invoice Attachemnt
                        </Label>
                        <a href={this.state.api} className="text-danger"> {this.state.invFileName} </a>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                        Invoice Number
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="MSH-ERE-344534" value={this.state.invoiceNo} type="text" autoComplete="" />
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Invoice Date 
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="Invoice Date" value={this.state.invoiceDate} type="text" autoComplete="" />
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Invoice Type
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="Invoice Type" type="text" value={this.state.invoiceType} autoComplete="" />
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label for="PaymentStatus">Branch GST Number</Label>
                        <Input type="select" name="select" id="PaymentStatus">
                         <option>Select </option>
                          <option> Chandigarh--------04ASDHT64WQ </option>
                          {/* <option> On Hold</option>
                          <option> In Process</option>
                          <option> Paid</option>
                          <option>Processed</option>
                          <option>Rejected</option>
                          <option>Document Received</option> */}

                        </Input>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label for="PaymentStatus">Vendor Code</Label>
                        <Input type="select" name="select" id="PaymentStatus">
                          <option> Select </option>
                          { this.state.vendorData.map((vendor,index) => 
                         <option>{vendor.vendorName}----------------{vendor.vendorCode}</option>
                          )}
                        </Input>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label for="PaymentStatus">TDS Category</Label>
                        <Input type="select" name="select" id="PaymentStatus">
                          <option> Select </option>
                          <option>Payment Of Rent For Plant/Machinery(194I)</option>
                          <option>Payment to Contractors(Individual/HUF)(194C)</option>
                          <option>Payment to Contractors(Other than Individual/HUF)(194C) </option>
                          <option>Payment of Brokerage and Commission(194H)</option>
                          <option>Payment of Rent For Land/Building/Furniture(194I)</option>
                          <option>Fees for Professional or Techincal Services(194J)</option>
                          <option>PAN not available(194C)</option>
                          <option>PAN not available(194J)</option>
                          <option>PAN not available(194I)</option>
                          <option>No TDS</option>
                        </Input>
                      </FormGroup>
                    </Col>
                  </Row>
                  <div className="custom-control custom-control-alternative custom-checkbox">
                  <h3 className="mb-0 text-16">
                  SUPPLIER GST REGISTERED:
                  </h3>
                  <Row>
                  <Col className="" lg="4">
                      <FormGroup>
                      <label >
                    <span className="">Yes</span>
                    </label>
                    <input type="checkbox" />
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                      <label >
                    <span className="">No</span>
                    </label>
                    <input type="checkbox" />
                      </FormGroup>
                    </Col>
                    </Row>
                  </div>
                  <div>
                    <Row>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                        HSN / SAC VALUE
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="00.00" type="text" autoComplete="" />
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                        GST TAXABLE VALUE:
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="0.0" type="text" autoComplete="" />
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                        TOTAL INVOICE VALUE
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="0.0" value={this.state.amount} type="text" autoComplete="" />
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                        NET PAYABLE AMOUNT:
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="0.0" type="text" autoComplete="" />
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="12">
                      <FormGroup>
                        <Label>
                          Narration
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="testing process" type="textarea" autoComplete="" />
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    </Row>
                  </div>
                  <div className="custom-control custom-control-alternative custom-checkbox">
                  <h3 className="mb-0 text-16">
                  AMOUNT OF TAX:
                  </h3>
                  <Row>
                  <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                        CGST
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="0.0" type="text" autoComplete="" />
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                        SGST
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="00.00" type="text" autoComplete="" />
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                        IGST
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="" type="text" autoComplete="" />
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    </Row>
                  </div>
                  <div className="custom-control custom-control-alternative custom-checkbox">
                  <h3 className="mb-0 text-16">
                  RATE OF TAX:
                  </h3>
                  <Row>
                  <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                        CGST
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="Abha Varma" type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                        SGST
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="Abha Varma" type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                        IGST
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="Abha Varma" type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    </Row>
                  </div>
                  <div className="custom-control custom-control-alternative custom-checkbox">
                  <h3 className="mb-0 text-16">
                  TDS:
                  </h3>
                  <Row>
                  <Col className="" lg="4">
                      <FormGroup>
                        <Label>             	
                         Deducted On
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="Abha Varma" type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                        	Amount
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="Abha Varma" type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                        	Percentage
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="Abha Varma" type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    </Row>
                  </div>
                </Form>
              </div>
            </Row>
              <Row>
                <Col className="text-center mt-4 pt-1" lg="12">
                  <Button className="bg-red border-0" type="button">
                    Confirm
                  </Button>
                  <Button className="bg-light-gray border-0" type="button">
                    Cancel
                  </Button>
                </Col>
              </Row> 
            </ModalBody>
          </Modal>
        </Container>
      </>
    );
  }
}

export default SupplierTrackerReport;
