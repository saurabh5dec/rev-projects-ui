
import React from "react";

// reactstrap components
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  FormText, 
  InputGroup,
  Modal,
  ModalHeader,
  ModalBody,
  Label
 
} from "reactstrap";
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';

import SupplierService from "service/SupplierService";

class SupplierForm extends React.Component {
  // confirmation Popup
  constructor(props) {
    super(props);
  // confirmation Popup
  this.state = {
    isOpen: false,
    isActive: false,
    userData:[],
    fromDate :'',
    selectValue:'',
    paymentMode:'',
    supplierType:'',
     amount:'',
     invoiceNumber:'',
     approvedBy:'',
     selectedFile:'',
     selectedFile2:'',
     description:'',
     beneficiaryName:'',
  };
  this.handleExpenseDropdownChange =this.handleExpenseDropdownChange.bind(this)
  this.fetchUserById=this.fetchUserById.bind(this);
  this.handleDueDate=this.handleDueDate.bind(this);
  this.handleModeOfPayment = this.handleModeOfPayment.bind(this);
  this.handleSupplier=this.handleSupplier.bind(this);
  this.handleAmout=this.handleAmout.bind(this);
  this.handleApprovedBy=this.handleApprovedBy.bind(this);
  this.handleBeneficiaryName=this.handleBeneficiaryName.bind(this);
  this.handleDescription=this.handleDescription.bind(this);
  this.handleInvoiceNumber=this.handleInvoiceNumber.bind(this);
  this.onFileChangeHandler=this.onFileChangeHandler.bind(this);
  this.onFileChangeHandler2=this.onFileChangeHandler2.bind(this);
  
}

 //Thanks Popup   

  toggleThanksModal = () => {
    this.setState({ isActive: !this.state.isActive });
  }

  componentDidMount() {
    this.fetchUserById();
}


  fetchUserById(){
  
    const user = JSON.parse(localStorage.getItem('user'));
    SupplierService.fetchUser(user.id)
        .then((res) => {
          this.setState({
            userData:res.data,
          });
        });
  }

  handleDueDate(day){
    this.setState({ fromDate: day });
  }


  //this function is used to select the location from dropdown 
handleExpenseDropdownChange(e){
  this.setState({ selectValue: e.target.value });
}

handleModeOfPayment(e){
  
  this.setState({ paymentMode: e.target.value }); 
}
handleSupplier(e){
  
  this.setState({ supplierType: e.target.value }); 
}

handleAmout(e){
  
  this.setState({ amount: e.target.value }); 
}

handleApprovedBy(e){
  
  this.setState({ approvedBy: e.target.value }); 
}

handleBeneficiaryName(e){

  this.setState({ beneficiaryName: e.target.value }); 
}

handleInvoiceNumber(e){
  
  this.setState({ invoiceNumber: e.target.value }); 
}

handleDescription(e){
  this.setState({ description: e.target.value }); 
}

onFileChangeHandler = (e) => {
  this.setState({
      selectedFile:  e.target.files[0],
  });

}


onFileChangeHandler2 = (e) => {

  this.setState({
      selectedFile2:  e.target.files[0],
  });

}




applySupplierForm =(e) =>{
  
  const formData = new FormData() 
  formData.append('file', this.state.selectedFile)
  formData.append('file', this.state.selectedFile2)
 const user = JSON.parse(localStorage.getItem('user'));
   SupplierService.upload(user.id,this.state.selectValue,this.state.amount,this.state.fromDate,
    this.state.paymentMode,this.state.supplierType,this.state.beneficiaryName,this.state.invoiceNumber,
    this.state.approvedBy,this.state.description,formData)
   .then((res) => {
     this.toggleThanksModal()
     this.setState({
       uniqueCode:res.data,
     });
     
   });
}









  render() {
    
    return (
      <>
       <div className="header pb-8 pt-3 pt-md-6">
          <Container fluid>
            <div className="header-body">
              {/* Card stats */}
              <Row>
                  <Col lg="12" xl="12">
                    <Card className="card-stats mb-4 mb-xl-0">
                      <CardBody>
                        <Row>
                          <div className="col">
                             <h3 className="mb-0">Employee Details</h3>
                            <Form role="form" className="mt-3 Custom-form">
                              <Row>
                                <Col className="" lg="3">
                                  <FormGroup className="mb-3">
                                    <Label>
                                      Employee Name
                                    </Label>
                                    <InputGroup className="input-group-alternative">
                                      <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                          <i className="ni ni-circle-08" />
                                        </InputGroupText>
                                      </InputGroupAddon>
                                      <Input placeholder="Saurabh Misra" value={this.state.userData.employeeName} type="text" autoComplete="" disabled/>
                                    </InputGroup>
                                  </FormGroup>
                                </Col>
                                <Col className="" lg="3">
                                  <FormGroup>
                                    <Label>
                                    Branch
                                    </Label>
                                    <InputGroup className="input-group-alternative">
                                      <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                          <i className="ni ni-paper-diploma" />
                                        </InputGroupText>
                                      </InputGroupAddon>
                                      <Input placeholder="Gurgaon-BO"  value={this.state.userData.branch} type="text" autoComplete="" disabled/>
                                    </InputGroup>
                                  </FormGroup>
                                </Col>
                                <Col className="" lg="3">
                                  <FormGroup>
                                    <Label>
                                      Email
                                    </Label>
                                    <InputGroup className="input-group-alternative">
                                      <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                          <i className="ni ni-email-83" />
                                        </InputGroupText>
                                      </InputGroupAddon>
                                      <Input placeholder="saurabh.misra@in.fcm.travel"  value={this.state.userData.email} type="email" autoComplete="" disabled/>
                                    </InputGroup>
                                  </FormGroup>
                                </Col>
                                <Col className="" lg="3">
                                  <FormGroup>
                                    <Label>
                                      Employee Id
                                    </Label>
                                    <InputGroup className="input-group-alternative">
                                      <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                          <i className="ni ni-badge" />
                                        </InputGroupText>
                                      </InputGroupAddon>
                                      <Input placeholder="FCM/IND/XXXX"  value={this.state.userData.employeeId} type="text" autoComplete="" disabled/>
                                    </InputGroup>
                                  </FormGroup>
                                </Col>
                              </Row>
                              {/* <div className="text-center mt-3">
                                <Button className="bg-red border-0" type="button" onClick={this.toggleModal}>
                                  Update
                                </Button>
                              </div> */}
                            </Form>
                          </div>
                          {/* <Col className="col-auto">
                            <div className="icon icon-shape bg-warning text-white rounded-circle shadow">
                              <i className="fas fa-plus" />
                            </div>
                          </Col> */}
                        </Row>
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
              </div>
           </Container>
        </div>
      {/* Page content */}
        <Container className="mt--7" fluid>
          <Row className="">
            <Col className="mb-5 mb-xl-0" xl="12">
              <Card className="shadow">
                <CardHeader className="border-0">
                  <Row className="align-items-center">
                    <div className="col">
                      <h3 className="mb-0">Fill the Details for Supplier</h3>
                    </div>
                  </Row>
                </CardHeader>
                <CardBody>
                    <Form>
                      <Row>
                        <Col className="" lg="3">
                          <FormGroup>
                            <Label for="FromDate">Due Date</Label>
                            <DayPickerInput  onDayChange={this.handleDueDate}/>
                          </FormGroup>
                        </Col>
                        <Col className="" lg="3">
                          <FormGroup>
                            <Label for="expenseSelect">Nature of Expense</Label>
                            <Input type="select" name="select" onChange={this.handleExpenseDropdownChange} id="expenseSelect">
                              <option placeholder=""> Select </option>
                              <option placeholder="Business Promotion">Business Promotion</option>
                              <option placeholder="Cellular Phone">Cellular Phone</option>
                              <option placeholder="Internet / Data Card">Internet / Data Card</option>
                              <option placeholder="Conveyance">Conveyance</option>
                              <option placeholder="Delivery">Delivery</option>
                              <option placeholder="Water / Tea / Coffee">Water / Tea / Coffee</option>
                              <option placeholder="Festival">Festival</option>
                              <option placeholder="Petty Cash">Petty Cash</option>
                              <option placeholder="Printing Stationery">Printing Stationery</option>
                              <option placeholder="Postage Courier">Postage Courier</option>
                              <option placeholder="Repair Maintenance - Computers">Repair  Maintenance - Computers</option>
                              <option placeholder="Repair Maintenance - Vehicles">Repair  Maintenance - Vehicles</option>
                              <option placeholder="Repair Maintenance - Building">Repair  Maintenance - Building</option>
                              <option placeholder="Repair Maintenance - Others">Repair  Maintenance - Others</option>
                              <option placeholder="Staff Welfare - Buzz Night">Staff Welfare - Buzz Night</option>
                              <option placeholder="Staff Welfare - In Office">Staff Welfare - In Office</option>
                              <option placeholder="Tea Meal">Tea Meal</option>
                              <option placeholder="Telephone">Telephone</option>
                              <option placeholder="Travelling - Domestic Others">Travelling - Domestic Others</option>
                              <option placeholder="Travelling - Foreign Others">Travelling - Foreign Others</option>
                              <option placeholder="Vehicle Parking">Vehicle Parking</option>
                              <option placeholder="Vehicle Petrol - Commercial">Vehicle Petrol - Commercial</option>
                            </Input>
                          </FormGroup>
                        </Col>
                        <Col className="" lg="3">
                          <FormGroup>
                            <Label for="modepayment">Mode Of Payment <span className="text-red">*</span></Label>
                            <Input type="select" name="select"  onChange={this.handleModeOfPayment} id="modepayment">
                              <option placeholder=""> Select </option>
                              <option placeholder=""> NEFT </option>
                              <option placeholder=""> Pay Order/ Cheque </option>
                            </Input>
                          </FormGroup>
                        </Col>
                        <Col className="" lg="3">
                          <FormGroup>
                            <Label for="modepayment">Supplier Type <span className="text-red">*</span></Label>
                            <Input type="select" name="select"  onChange={this.handleSupplier} id="modepayment">
                              <option placeholder=""> Select </option>
                              <option placeholder=""> Existing Supplier </option>
                              <option placeholder=""> New Supplier </option>
                            </Input>
                          </FormGroup>
                        </Col>
                        <Col className="" lg="3">
                          <FormGroup>
                            <Label>Beneficiary Name <span className="text-red">*</span></Label>
                            <Input
                              type="text"
                              placeholder="Beneficiary Name"
                              onChange={this.handleBeneficiaryName}
                            />
                          </FormGroup>
                        </Col>
                        <Col className="" lg="3">
                            <FormGroup>
                              <Label for="amount">Amount <span className="text-red">*</span></Label>
                              <Input
                                type="number"
                                name="amount"
                                placeholder="Amount"
                                onChange={this.handleAmout}
                              />
                            </FormGroup>
                          </Col>
                          <Col className="" lg="3">
                            <FormGroup>
                              <Label>Invoice Number (Optional)</Label>
                              <Input
                                type="text"
                                placeholder="Invoice Number"
                                onChange={this.handleInvoiceNumber}
                              />
                            </FormGroup>
                          </Col>
                          <Col className="" lg="3">
                          <FormGroup>
                            <Label>Approved By <span className="text-red">*</span></Label>
                            <Input
                              type="text"
                              placeholder="Approved By"
                              onChange={this.handleApprovedBy}
                            />
                          </FormGroup>
                        </Col>
                          <Col lg="12">
                            <FormGroup>
                              <Label for="description">Description <span className="text-red">*</span></Label>
                              <Input type="textarea" onChange={this.handleDescription} name="text" id="description" />
                            </FormGroup>
                          </Col>
                          <Col lg="12">
                          <h3>Upload attachment <span className="text-red">*</span> <small>(Online mode is mandatory now)</small>
                          </h3>
                          </Col>
                          <Col lg="6">
                           <FormGroup>
                              <Label for="description">Approval Attachment <span className="text-red">*</span></Label>
                              <Input type="file" name="file"  onChange={this.onFileChangeHandler} id="Upload" />
                              <FormText color="red">
                                  Note:- Approved by attachment (in .pdf, .xls/x, .doc/x format only)
                              </FormText>
                            </FormGroup>
                          </Col>
                          <Col lg="6">
                           <FormGroup>
                              <Label for="description">Invoice Attachment<span className="text-red">*</span></Label>
                              <Input type="file" name="file" onChange={this.onFileChangeHandler2} id="Upload" />
                              <FormText color="red">
                                Note:- 	Attachment is mandatory for Utility Bills (in .pdf, .xls/x, .doc/x format only)
                              </FormText>
                            </FormGroup>
                          </Col>
                      </Row>
                    </Form>
                </CardBody>
                <div className="text-center mt-3 mb-3">
                  <Button className="bg-red border-0" type="button" onClick={this.applySupplierForm}>
                    Submit
                  </Button>
                </div>
                {/* Thanks modalbox */}
                <Modal className=""  isOpen={this.state.isActive}>
                  <ModalHeader className="bg-light-gray" toggle={this.toggleThanksModal}>
                    <h3 className="mb-0 text-16">
                      Thanks For Using Expense Claim Tool
                    </h3>
                  </ModalHeader>
                  <ModalBody className="pt-0 text-center">
                    <div className="icon icon-shape bg-success text-white rounded-circle shadow mb-4 mt-3">
                        <i className="ni ni-check-bold" />
                    </div>
                        <h5 className="text-gray mb-4">Your Unique Refrence ID is <b className="text-success text-16">{this.state.uniqueCode}</b>. Voucher Submited is Offline. To check Status <a href="#test" className="text-blue">Click Here</a></h5>
                   
                  </ModalBody>
                </Modal>
              </Card>
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

export default SupplierForm;
