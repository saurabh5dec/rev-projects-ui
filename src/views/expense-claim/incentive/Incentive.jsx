
import React from "react";

// reactstrap components
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Badge,
  Label,
  Table
 
} from "reactstrap";

class Incentive extends React.Component {

  render() {
    return (
      <>
       {/* Detail */}
        {/* Search Form */}
       <div className="header pb-3 pt-4 pt-md-4">
          <Container fluid>
            <div className="header-body">
              {/* Card stats */}
              <Row>
                  <Col lg="12" xl="12">
                    <Card className="card-stats mb-4 mb-xl-0">
                      <CardBody>
                        <Row>
                          <div className="col">
                             <h3 className="mb-0">Incentive</h3>
                            <Form role="form" className="mt-3 Custom-form">
                              <Row>
                                <Col className="" lg="6">
                                  <FormGroup>
                                    <Label for="PaymentStatus">Select Month</Label>
                                    <Input type="select" name="select" id="">
                                      <option> July - 20 </option>
                                      <option> Aug - 20 </option>
                                    </Input>
                                  </FormGroup>
                                </Col>
                                <Col className="" lg="6">
                                    <FormGroup>
                                      <Label for="tcname">TC Name </Label>
                                      <Input
                                        type="text"
                                        name="tcname"
                                        placeholder="Tc Name"
                                      />
                                    </FormGroup>
                                  </Col>
                              </Row>
                              <div className="text-center mt-3">
                                <Button className="bg-red border-0" type="button">
                                  Submit
                                </Button>
                              </div>
                            </Form>
                          </div>
                        </Row>
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
              </div>
           </Container>
        </div>
      {/* Page content */}
      
        <Container className="mt-3" fluid>
          <Row className="">
            <Col className="mb-5 mb-xl-0" xl="12">
              <Card className="shadow">
                <CardHeader className="border-0 pb-0">
                  <Row className="align-items-center">
                    <div className="col">
                      <h3 className="mb-0">Incentive Tracking Report</h3>
                    </div>
                    <div className="text-center mt-3">
                      <Button className="bg-red border-0" type="button">
                        <i className="fas fa-download mr-2"></i>
                        Export Excel
                      </Button>
                    </div>
                  </Row>
                </CardHeader>
                <CardBody>
                  <Table className="align-items-center table-flush" responsive>
                    <thead className="thead-light">
                      <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Branch</th>
                        <th scope="col">Emp. ID</th>
                        <th scope="col">Amount</th>
                        <th scope="col">Approved By</th>
                        <th scope="col">Status</th>
                        {/* <th scope="col">Print</th>
                        <th scope="col">Detail</th> */}
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row">1</th>
                        <td>Saurabh Misra</td>
                        <td>Noida HO</td>
                        <td>FCM/IND/XXXX</td>
                        <td>2000</td>
                        <td>Abha Varma</td>
                        <td><Badge color="" className="badge-dot mr-4">
                          <i className="bg-warning" />
                          pending
                        </Badge></td>
                        {/* <td><a href="#top"><i className="fas fa-print"  aria-hidden="true"></i> Print</a></td>
                        <td><a href="#top" className="text-success"><i className="fas fa-eye" aria-hidden="true"></i> View</a></td> */}
                      </tr>
                      <tr>
                        <th scope="row">2</th>
                        <td>Saurabh Misra</td>
                        <td>Noida HO</td>
                        <td>FCM/IND/XXXX</td>
                        <td>2000</td>
                        <td>Abha Varma</td>
                        <td><Badge color="" className="badge-dot mr-4">
                          <i className="bg-warning" />
                          pending
                        </Badge></td>
                        {/* <td><a href="#top"><i className="fas fa-print"  aria-hidden="true"></i> Print</a></td>
                        <td><a href="#top" className="text-success"><i class="fas fa-eye" aria-hidden="true"></i> View</a></td> */}
                      </tr>
                      
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

export default Incentive;
