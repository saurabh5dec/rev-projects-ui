
import React from "react";

// reactstrap components
import {
  Button,
  Card,
  CardTitle,
  CardBody,
  CardHeader,
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,

  Label,
  Table,
  Modal,
  ModalHeader,ModalBody,InputGroup,
 
} from "reactstrap";
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import SupplierService from "service/SupplierService";
import ReactPaginate from 'react-paginate';
import moment from 'moment';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import { formatDate, parseDate } from 'react-day-picker/moment'

class VendorTrackerReport extends React.Component {
  constructor(props) {
    super(props);
    this.handleFromChange = this.handleFromChange.bind(this);
    this.handleToChange = this.handleToChange.bind(this);
    this.findAllPendingRequest=this.findAllPendingRequest.bind(this)
    this.toggleModal= this.toggleModal.bind(this);


    this.state = {
      from: undefined,
      to: undefined,
      userData:[],
      requestFormData:[],
      uniqueCode:'',
      status:'',
      name:'',
      offset: 0,
      perPage: 10,
      currentPage: 0,
      tableData:[],
      isOpen:false,
      id:0,
      bankAccount:'',
      vendorAddress:'',
      ifscCode:'',
    };

    this.handleVendorAddress=this.handleVendorAddress.bind(this);
    this.handleVendorBankAccount=this.handleVendorBankAccount.bind(this);
    this.handleVendorIfscCode=this.handleVendorIfscCode.bind(this);

  }

 

  handleVendorAddress=(e)=>{
    this.setState({ vendorAddress: e.target.value });
  }
  handleVendorBankAccount=(e)=>{
    this.setState({ bankAccount: e.target.value });
  }
  
  handleVendorIfscCode=(e)=>{
    this.setState({ ifscCode: e.target.value });
  }











  toggleModal = (requestFormData) => {
    
     this.setState({ 
       isOpen: !this.state.isOpen,
       id:requestFormData.id,
       vendorName:requestFormData.vendorName,
       subdate:requestFormData.subdate,
       vendorCode:requestFormData.vendorCode,
      gst:requestFormData.gst,
      pan:requestFormData.pan,
      vendorAddress:requestFormData.vendorAddress,
      bankAccount:requestFormData.bankAccount,
      ifscCode:requestFormData.ifscCode
     });
   };







  showFromMonth() {
    const { from, to } = this.state;
    if (!from) {
      return;
    }
    if (moment(to).diff(moment(from), 'months') < 2) {
      this.to.getDayPicker().showMonth(from);
    }
  }

  handleFromChange(from) {
    // Change the from date and focus the "to" input field
    this.setState({ from });
  }

  handleToChange(to) {
    this.setState({ to }, this.showFromMonth);
  }

  componentDidMount() {
    
    this.findAllPendingRequest();
}



findAllPendingRequest(){
    SupplierService.findAllVendor()
        .then((res) => {
          var data = res.data;
          var slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)
          this.setState({
            requestFormData:res.data,
            pageCount: Math.ceil(data.length / this.state.perPage),
            tableData:slice,
          })
      });
          
}





searchData =(e) =>{

  // ExpenseService.findData(this.state.from,this.state.to,this.state.uniqueCode,
  //   this.state.status,this.state.name)
  // .then((res) => {
  //   this.setState({
  //     requestFormData:res.data,
  //   });
  // });
}





handlePageClick = (e) =>{
  const selectedPage = e.selected;
  const offset= selectedPage * this.state.perPage;
  this.setState({
    currentPage:selectedPage,
    offset:offset
  }, () =>{
    this.loadMoreData();
   });
}


loadMoreData(){
  const data=this.state.requestFormData;
  const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)
  this.setState({
    pageCount: Math.ceil(data.length / this.state.perPage),
    tableData:slice,
  });
}

deleteVendorClicked(id) {

  SupplierService.deleteVendor(id)
      .then(
          response => {
            // swal({
            //   title:"Success",
            //   text:"Product Deleted sucessfully",
            //   icon:"success",
            //   timer:2000,
            //   buttons:false,
            // })
            
              this.findAllPendingRequest()
          }
      )

}


updateStatus=(e)=>{
const vendor = {id:this.state.id,bankAccount:this.state.bankAccount,ifscCode:this.state.ifscCode,vendorAddress:this.state.vendorAddress}
SupplierService.UpdateVendor(vendor)
   .then((res) => {
 
});
}


  render() {
    const { from, to } = this.state;
    const modifiers = { start: from, end: to };
    return (
      <>
      
      {/* Search Form */}
        <div className="header pb-3 pt-4 pt-md-4">
          <Container fluid>
            <div className="header-body">
              {/* Card stats */}
              <Row>
                  <Col lg="12" xl="12">
                    <Card className="card-stats mb-4 mb-xl-0">
                      <CardBody>
                        <Row>
                          <div className="col">
                             <h3 className="mb-0">Details</h3>
                            <Form role="form" className="mt-3 Custom-form">
                              <Row>
                                <Col className="Subfrom-to" lg="12">
                                  <Row>
                                    <Col className="" lg="6">
                                      <FormGroup>
                                        <Label for="FromDate">Submission Date From</Label><br></br>
                                        <DayPickerInput type="date"
                                        value={from}
                                        placeholder="From"
                                        format="LL"
                                        formatDate={formatDate}
                                        parseDate={parseDate}
                                        dayPickerProps={{
                                          selectedDays: [from, { from, to }],
                                          disabledDays: { after: to },
                                          toMonth: to,
                                          modifiers,
                                          numberOfMonths: 2,
                                          onDayClick: () => this.to.getInput().focus(),
                                        }}
                                        onDayChange={this.handleFromChange}
                                      />
                                      </FormGroup>
                                    </Col>
                                    <Col className="" lg="6">
                                      <FormGroup>
                                        <Label for="ToDate">To</Label><br></br>
                                        <DayPickerInput type="text" 
                                          ref={el => (this.to = el)}
                                          value={to}
                                          placeholder="To"
                                          format="LL"
                                          formatDate={formatDate}
                                          parseDate={parseDate}
                                          dayPickerProps={{
                                            selectedDays: [from, { from, to }],
                                            disabledDays: { before: from },
                                            modifiers,
                                            month: from,
                                            fromMonth: from,
                                            numberOfMonths: 2,
                                          }}
                                          onDayChange={this.handleToChange}
                                        />
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                </Col>
                                <Col className="" lg="4">
                                    <FormGroup>
                                      <Label for="referenceid">Vendor Name</Label>
                                      <Input
                                        type="number"
                                        name="referenceid"
                                        placeholder="vendor name"
                                        onChange={this.UniqueCodeHandle}
                                      />
                                    </FormGroup>
                                  </Col>
                              </Row>
                              <div className="text-center mt-3">
                                <Button onClick={this.searchData} className="bg-red border-0" type="button">
                                  Search
                                </Button>
                              </div>
                            </Form>
                          </div>
                        </Row>
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
              </div>
           </Container>
        </div>
      {/* Page content */}
      
        <Container className="mt-3" fluid>
          <Row className="">
            <Col className="mb-5 mb-xl-0" xl="12">
              <Card className="shadow">
                <CardHeader className="border-0 pb-0">
                  <Row className="align-items-center">
                    <div className="col">
                      <h3 className="mb-0">Vendor Tracking Report</h3>
                    </div>
                    {/* <div className="text-center mt-3">
                      <Button className="bg-red border-0" type="button">
                        <i className="fas fa-download mr-2"></i>
                        Export Excel
                      </Button>
                      <Button className="bg-red border-0" type="button">
                        <i className="fas fa-download mr-2"></i>
                        Export Master Excel
                      </Button>
                    </div>  */}
                    <div className="text-center mt-3">  
                                        <ReactHTMLTableToExcel  
                                                className="btn btn-info"  
                                                table="vendor"  
                                                filename="ReportExcel"  
                                                sheet="Sheet"  
                                                buttonText="Export excel" />  
                       </div>  
                  </Row>
                </CardHeader>
                <CardBody>
                  <Table  id="vendor" className="align-items-center table-flush" responsive>
                    <thead className="thead-light">
                      <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Submission Date</th>
                        <th scope="col">Vendor Name</th>
                        <th scope="col">Vendor Code</th>
                        <th scope="col">Pan</th>
                        <th scope="col">Gst</th>
                        <th scope="col">Bank Account</th>
                        <th scope="col">IFSC Code</th>
                        <th scope="col">Vendor Address</th>
                        <th scope="col">Edit</th>
                        <th scope="col">Delete</th>
                      </tr>
                    </thead>
                    <tbody>
                    { this.state.tableData.map((requestFormData,index) => 
                    <tr key={requestFormData.id}>
                       <td>{requestFormData.id}</td>
                       <td>{requestFormData.subdate}</td>
                       <td>{requestFormData.vendorName}</td>
                       <td>{requestFormData.vendorCode}</td>
                       <td>{requestFormData.pan}</td>
                      <td>{requestFormData.gst}</td>
                      <td>{requestFormData.bankAccount}</td> 
                      <td>{requestFormData.ifscCode}</td> 
                      <td>{requestFormData.vendorAddress}</td>
                      <td>
                      <Form id='table-editaction' method='' action='#'>
                      <Button className="text-success p-0" >
                              <i className="fas fa-edit text-16" onClick={(e) => this.toggleModal(requestFormData)}  />
                            </Button>
                             <Button className="text-danger p-0">
                               <i className="fas fa-trash text-16" onClick={() => this.deleteVendorClicked(requestFormData.id)}/>
                            </Button>
                        </Form>
                      </td>
                      </tr>
                    )}



                 
                    </tbody>
                  </Table>
                  <ReactPaginate
                    previousLabel={"prev"}
                    nextLabel={"next"}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={this.state.pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={this.handlePageClick}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"}/>
                </CardBody>
              </Card>
            </Col>
          </Row>
           {/* View modalbox */}
           <Modal className="modal-size"  isOpen={this.state.isOpen}>
            <ModalHeader className="bg-light-gray" toggle={this.toggleModal}>
              <h3 className="mb-0 text-16">
                Edit Vendor
              </h3>
            </ModalHeader>
            <ModalBody className="pt-0">
            <Row>
              <div className="col">
                <Form role="form" className="mt-3 Custom-form">
                  <Row>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Refrence ID 
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="809828909" value={this.state.id} type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Submission Date 
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="" value={this.state.subdate} type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup> 
                        <Label>
                          Vendor Name 
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="3000" value={this.state.vendorName} type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Vendor Code
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="Abha Varma" value={this.state.vendorCode} type="text" autoComplete=""  disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Pan
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="Pending" value={this.state.pan} type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Gst
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="Test Data"  value={this.state.gst}  type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Bank Account
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="Test Data"  onChange={this.handleVendorBankAccount} defaultValue={this.state.bankAccount} type="text" autoComplete=""/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          IFSC Code
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="Test Data" onChange={this.handleVendorIfscCode} defaultValue={this.state.ifscCode} type="text" autoComplete=""/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Vendor Address
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="Test Data" onChange={this.handleVendorAddress} defaultValue={this.state.vendorAddress} type="text" autoComplete=""/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
              </div>
            </Row>
              <Row>
                <Col className="text-center mt-4 pt-1" lg="12">
                  <Button onClick={this.updateStatus} className="bg-red border-0" type="button">
                    Confirm
                  </Button>
                  <Button className="bg-light-gray border-0" type="button">
                    Cancel
                  </Button>
                </Col>
              </Row> 
            </ModalBody>
          </Modal>

        </Container>
      </>
    );
  }
}

export default VendorTrackerReport;
