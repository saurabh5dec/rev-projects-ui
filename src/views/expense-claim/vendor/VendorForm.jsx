
import React from "react";

// reactstrap components
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  FormText, 
  InputGroup,
  Modal,
  ModalHeader,
  ModalBody,
  Label
 
} from "reactstrap";
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import SupplierService from "service/SupplierService";
import swal from 'sweetalert';

class VendorForm extends React.Component {
  // confirmation Popup
  constructor(props) {
    super(props);
  // confirmation Popup
  this.state = {
    isOpen: false,
    isActive: false,
    vendorName:'',
    vendorCode:'',
    gst:'',
    pan:'',
    bankAccount:'',
    vendorEmail:'',
    vendorContacts:'',
    vendorAddress:'',
    ifscCode:'',
  };
 this.handleVendorAddress=this.handleVendorAddress.bind(this);
 this.handleVendorBankAccount=this.handleVendorBankAccount.bind(this);
 this.handleVendorCode=this.handleVendorCode.bind(this);
 this.handleVendorContacts=this.handleVendorContacts.bind(this);
 this.handleVendorEmail=this.handleVendorEmail.bind(this);
 this.handleVendorGst=this.handleVendorGst.bind(this);
 this.handleVendorName=this.handleVendorName.bind(this);
 this.handleVendorPan=this.handleVendorPan.bind(this);
 this.handleVendorIfscCode=this.handleVendorIfscCode.bind(this);
}

 //Thanks Popup   

  toggleThanksModal = () => {
    this.setState({ isActive: !this.state.isActive });
  }

  handleVendorName=(e)=>{
    this.setState({ vendorName: e.target.value });
  }

  handleVendorCode=(e)=>{
    this.setState({ vendorCode: e.target.value });
  }

  handleVendorAddress=(e)=>{
    this.setState({ vendorAddress: e.target.value });
  }
  handleVendorBankAccount=(e)=>{
    this.setState({ bankAccount: e.target.value });
  }
  handleVendorContacts=(e)=>{
    this.setState({ vendorContacts: e.target.value });
  }

  handleVendorEmail=(e)=>{
    this.setState({ vendorEmail: e.target.value });
  }
  handleVendorGst=(e)=>{
    this.setState({ gst: e.target.value });
  }
  handleVendorPan=(e)=>{
    this.setState({ pan: e.target.value });
  }
  
  handleVendorIfscCode=(e)=>{
    this.setState({ ifscCode: e.target.value });
  }
  componentDidMount() {
    
}


addVendor=(e) =>{
const vendor = {vendorName:this.state.vendorName,vendorCode:this.state.vendorCode,pan:this.state.pan,
  gst:this.state.gst,bankAccount:this.state.bankAccount,ifscCode:this.state.ifscCode,vendorAddress:this.state.vendorAddress,
vendorEmail:this.state.vendorEmail,vendorContacts:this.state.vendorContacts}
alert(JSON.stringify(vendor))
SupplierService.addVendor(vendor)
   .then((res) => {
    swal({
      title:"Success",
      text:"Vendor Added sucessfully",
      icon:"success",
      timer:2000,
      buttons:false,
    })
   

});
}
 
  render() {
    
    return (
      <>
       
      {/* Page content */}
      <div className="header pb-8 pt-3 pt-md-6">
      <Container fluid>
            <div className="header-body">
              {/* Card stats */}
              <Row>
                  <Col lg="12" xl="12">
                    <Card className="card-stats mb-4 mb-xl-0">
                      <CardBody>
                        <Row>
                          <div className="col">
                             <h3 className="mb-0">Vendor Form</h3>
                            <Form role="form" className="mt-3 Custom-form">
                              <Row>
                                <Col className="" lg="3">
                                  <FormGroup className="mb-3">
                                    <Label>
                                      Vendor Name
                                    </Label>
                                    <InputGroup className="input-group-alternative">
                                      <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                          <i className="ni ni-circle-08" />
                                        </InputGroupText>
                                      </InputGroupAddon>
                                      <Input placeholder="Saurabh Misra" onChange={this.handleVendorName}  type="text" autoComplete="" />
                                    </InputGroup>
                                  </FormGroup>
                                </Col>
                                <Col className="" lg="3">
                                  <FormGroup>
                                    <Label>
                                    Vendor Code
                                    </Label>
                                    <InputGroup className="input-group-alternative">
                                      <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                          <i className="ni ni-paper-diploma" />
                                        </InputGroupText>
                                      </InputGroupAddon>
                                      <Input placeholder="123456" onChange={this.handleVendorCode}  type="text" autoComplete="" />
                                    </InputGroup>
                                  </FormGroup>
                                </Col>
                                <Col className="" lg="3">
                                  <FormGroup>
                                    <Label>
                                      Vendor Pan
                                    </Label>
                                    <InputGroup className="input-group-alternative">
                                      <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                          <i className="ni ni-email-83" />
                                        </InputGroupText>
                                      </InputGroupAddon>
                                      <Input placeholder="DANP8989C" onChange={this.handleVendorPan}   type="text" autoComplete="" />
                                    </InputGroup>
                                  </FormGroup>
                                </Col>
                                <Col className="" lg="3">
                                  <FormGroup>
                                    <Label>
                                      Gst
                                    </Label>
                                    <InputGroup className="input-group-alternative">
                                      <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                          <i className="ni ni-badge" />
                                        </InputGroupText>
                                      </InputGroupAddon>
                                      <Input placeholder="FCM/IND/XXXX" onChange={this.handleVendorGst}   type="text" autoComplete="" />
                                    </InputGroup>
                                  </FormGroup>
                                </Col>
                                <Col className="" lg="3">
                                  <FormGroup>
                                    <Label>
                                      Bank Account Number
                                    </Label>
                                    <InputGroup className="input-group-alternative">
                                      <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                          <i className="ni ni-badge" />
                                        </InputGroupText>
                                      </InputGroupAddon>
                                      <Input placeholder="7868768890333"  onChange={this.handleVendorBankAccount}  type="text" autoComplete="" />
                                    </InputGroup>
                                  </FormGroup>
                                </Col>
                                <Col className="" lg="3">
                                  <FormGroup>
                                    <Label>
                                      IFSC Code
                                    </Label>
                                    <InputGroup className="input-group-alternative">
                                      <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                          <i className="ni ni-badge" />
                                        </InputGroupText>
                                      </InputGroupAddon>
                                      <Input placeholder="IFSC00000"  onChange={this.handleVendorIfscCode} type="text" autoComplete="" />
                                    </InputGroup>
                                  </FormGroup>
                                </Col>
                                <Col className="" lg="3">
                                  <FormGroup>
                                    <Label>
                                      Vendor Address
                                    </Label>
                                    <InputGroup className="input-group-alternative">
                                      <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                          <i className="ni ni-badge" />
                                        </InputGroupText>
                                      </InputGroupAddon>
                                      <Input placeholder="Pune" onChange={this.handleVendorAddress}  type="text" autoComplete="" />
                                    </InputGroup>
                                  </FormGroup>
                                </Col>
                                <Col className="" lg="3">
                                  <FormGroup>
                                    <Label>
                                      Vendor Email
                                    </Label>
                                    <InputGroup className="input-group-alternative">
                                      <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                          <i className="ni ni-badge" />
                                        </InputGroupText>
                                      </InputGroupAddon>
                                      <Input placeholder="test@gmail.com" onChange={this.handleVendorEmail}  type="email" autoComplete="" />
                                    </InputGroup>
                                  </FormGroup>
                                </Col>
                                <Col className="" lg="3">
                                  <FormGroup>
                                    <Label>
                                      Vendor Contacts
                                    </Label>
                                    <InputGroup className="input-group-alternative">
                                      <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                          <i className="ni ni-badge" />
                                        </InputGroupText>
                                      </InputGroupAddon>
                                      <Input placeholder="0000000000" onChange={this.handleVendorContacts}  type="text" autoComplete="" />
                                    </InputGroup>
                                  </FormGroup>
                                </Col>
                              </Row>
                             
                            </Form>
                          </div>
                          
                        </Row>
                      </CardBody>
                      <div className="text-center mt-3 mb-3">
                    <Button className="bg-red border-0" onClick={this.addVendor} type="button">
                        Submit
                  </Button>
                </div>
                    </Card>
                  </Col>
                </Row>
              </div>
           </Container>
      </div>
  
      </>
    );
  }
}

export default VendorForm;
