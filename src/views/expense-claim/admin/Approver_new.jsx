
import React from "react";
// reactstrap components
import {
  Button,
  Card,
  CardTitle,
  CardBody,
  CardHeader,
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Badge,
  Label,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  InputGroup,
 
} from "reactstrap";
import moment from 'moment';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import { formatDate, parseDate } from 'react-day-picker/moment'

class Approver extends React.Component {
  constructor(props) {
    super(props);
    this.handleFromChange = this.handleFromChange.bind(this);
    this.handleToChange = this.handleToChange.bind(this);
    this.state = {
      from: undefined,
      to: undefined,
    };
  }
  showFromMonth() {
    const { from, to } = this.state;
    if (!from) {
      return;
    }
    if (moment(to).diff(moment(from), 'months') < 2) {
      this.to.getDayPicker().showMonth(from);
    }
  }

  handleFromChange(from) {
    // Change the from date and focus the "to" input field
    this.setState({ from });
  }

  handleToChange(to) {
    this.setState({ to }, this.showFromMonth);
  }
// View Popup
state = {
  isOpen: false
};

toggleModal = () => {
  this.setState({ isOpen: !this.state.isOpen });
};
  render() {
    const { from, to } = this.state;
    const modifiers = { start: from, end: to };
    return (
      <>
       {/* Detail */}
       <div className=" pb-2 pt-5 pt-md-5">
          <Container fluid>
            <div className="">
              {/* Card stats */}
              <Row>
                <Col lg="6" xl="6">
                  <Card className="card-stats mb-4 mb-xl-0">
                    <CardBody>
                      <Row>
                        <div className="col">
                          <CardTitle
                            tag="h5"
                            className="text-uppercase text-muted mb-0"
                          >
                          Status
                          </CardTitle>
                          <span className="h5 font-weight-bold mb-0">
                          Pending List: <span>30 Queries</span>
                          </span>
                          <br></br>
                          <span className="h5 font-weight-bold mb-0">
                           On Hold : <span>5 Queries</span>
                          </span>
                        </div>
                        <Col className="col-auto">
                          <div className="icon icon-shape bg-yellow text-white rounded-circle shadow">
                            <i className="ni ni-check-bold" />
                          </div>
                        </Col>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
                <Col lg="6" xl="6">
                  <Card className="card-stats mb-4 mb-xl-0">
                    <CardBody>
                      <Row>
                        <div className="col">
                          <CardTitle
                            tag="h5"
                            className="text-uppercase text-muted mb-0"
                          >
                            Approved Details
                          </CardTitle>
                          <span className="h5 font-weight-bold mb-0">
                            Approved List : <span>50 Queries</span>
                          </span>
                          <br></br>
                          <span className="h5 font-weight-bold mb-0">
                           Rejected  : <span>10 Queries</span>
                          </span>
                        </div>
                        <Col className="col-auto">
                          <div className="icon icon-shape bg-success text-white rounded-circle shadow">
                            <i className="ni ni-check-bold" />
                          </div>
                        </Col>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </div>
          </Container>
        </div> 
        {/* Search Form */}
       <div className="header pb-3 pt-4 pt-md-4">
          <Container fluid>
            <div className="header-body">
              {/* Card stats */}
              <Row>
                  <Col lg="12" xl="12">
                    <Card className="card-stats mb-4 mb-xl-0">
                      <CardBody>
                        <Row>
                          <div className="col">
                             <h3 className="mb-0">Search Detail</h3>
                            <Form role="form" className="mt-3 Custom-form">
                              <Row>
                              <Col className="Subfrom-to" lg="12">
                                  <Row>
                                    <Col className="" lg="6">
                                      <FormGroup>
                                        <Label for="FromDate">Submission Date From</Label><br></br>
                                        <DayPickerInput type="date"
                                        value={from}
                                        placeholder="From"
                                        format="LL"
                                        formatDate={formatDate}
                                        parseDate={parseDate}
                                        dayPickerProps={{
                                          selectedDays: [from, { from, to }],
                                          disabledDays: { after: to },
                                          toMonth: to,
                                          modifiers,
                                          numberOfMonths: 2,
                                          onDayClick: () => this.to.getInput().focus(),
                                        }}
                                        onDayChange={this.handleFromChange}
                                      />
                                      </FormGroup>
                                    </Col>
                                    <Col className="" lg="6">
                                      <FormGroup>
                                        <Label for="ToDate">To</Label><br></br>
                                        <DayPickerInput type="text" 
                                          ref={el => (this.to = el)}
                                          value={to}
                                          placeholder="To"
                                          format="LL"
                                          formatDate={formatDate}
                                          parseDate={parseDate}
                                          dayPickerProps={{
                                            selectedDays: [from, { from, to }],
                                            disabledDays: { before: from },
                                            modifiers,
                                            month: from,
                                            fromMonth: from,
                                            numberOfMonths: 2,
                                          }}
                                          onDayChange={this.handleToChange}
                                        />
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                </Col>
                                <Col className="" lg="4">
                                    <FormGroup>
                                      <Label for="referenceid">System ID Number</Label>
                                      <Input
                                        type="number"
                                        name="referenceid"
                                        placeholder="System ID Number"
                                      />
                                    </FormGroup>
                                  </Col>
                                <Col className="" lg="4">
                                  <FormGroup>
                                    <Label for="PaymentStatus">Status</Label>
                                    <Input type="select" name="select" id="PaymentStatus">
                                      <option> Pending </option>
                                      <option> On Hold</option>
                                      <option> In Process</option>
                                      <option> Paid</option>
                                      <option>Processed</option>
                                      <option>Rejected</option>
                                      <option>Document Received</option>

                                    </Input>
                                  </FormGroup>
                                </Col>
                                <Col className="" lg="4">
                                    <FormGroup>
                                      <Label for="Employeename">Employee Name </Label>
                                      <Input
                                        type="text"
                                        name="Employeename"
                                        placeholder="Employee Name"
                                      />
                                    </FormGroup>
                                  </Col>
                              </Row>
                              <div className="text-center mt-3">
                                <Button className="bg-red border-0" type="button">
                                  Search
                                </Button>
                              </div>
                            </Form>
                          </div>
                        </Row>
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
              </div>
           </Container>
        </div>
      {/* Page content */}
      
        <Container className="mt-3" fluid>
          <Row className="">
            <Col className="mb-5 mb-xl-0" xl="12">
              <Card className="shadow">
                <CardHeader className="border-0 pb-0">
                  <Row className="align-items-center">
                    <div className="col">
                      <h3 className="mb-0">Tracking Report</h3>
                    </div>
                    <div className="text-center mt-3">
                      <Button className="bg-red border-0" type="button">
                        <i className="fas fa-download mr-2"></i>
                        Export Excel
                      </Button>
                    </div>
                  </Row>
                </CardHeader>
                <CardBody>
                  <Table className="align-items-center table-flush" responsive>
                    <thead className="thead-light">
                      <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Sub Date</th>
                        <th scope="col">Name</th>
                        <th scope="col">Branch</th>
                        <th scope="col">Emp. ID</th>
                        <th scope="col">Amount (INR)</th>
                        <th scope="col">Approved By</th>
                        <th scope="col">Approved Attachemnt</th>
                        <th scope="col">Invoice Attachemnt</th>
                        <th scope="col">Finance Status</th>
                        <th scope="col">Approver Status</th>
                        <th scope="col">Print</th>
                        <th scope="col">Detail</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row">1</th>
                        <td>12-08-2020</td>
                        <td>Saurabh Misra</td>
                        <td>Noida HO</td>
                        <td>FCM/IND/XXXX</td>
                        <td>2000</td>
                        <td>Abha Varma</td>
                        <td>attachment</td>
                        <td>attachment1</td>
                        <td><Badge color="" className="badge-dot mr-4">
                          <i className="bg-warning" />
                          pending
                        </Badge></td>
                        <td>
                            <Input type="select" className="p-1" name="select" id="ApproverStatus">
                              <option> Pending </option>
                              <option> On Hold</option>
                              <option> In Process</option>
                              <option> Paid</option>
                              <option>Processed</option>
                              <option>Rejected</option>

                            </Input>
                        </td>
                        <td><Button  type="button"  className="text-primary shadow-none p-0" onClick={() => this.props.history.push('/admin/print')}><i className="fas fa-print"  aria-hidden="true"></i> Print</Button></td>
                        <td><Button  type="button" onClick={this.toggleModal} className="text-success shadow-none p-0"><i class="fas fa-eye" aria-hidden="true"></i> View</Button></td>
                      </tr>
                      <tr>
                        <th scope="row">2</th>
                        <td>12-08-2020</td>
                        <td>Saurabh Misra</td>
                        <td>Noida HO</td>
                        <td>FCM/IND/XXXX</td>
                        <td>2000</td>
                        <td>Abha Varma</td>
                        <td>attachment</td>
                        <td>attachment1</td>
                        <td><Badge color="" className="badge-dot mr-4">
                          <i className="bg-warning" />
                          pending
                        </Badge></td>
                        <td>
                            <Input type="select" className="p-1" name="select" id="ApproverStatus">
                              <option> Pending </option>
                              <option> On Hold</option>
                              <option> In Process</option>
                              <option> Paid</option>
                              <option>Processed</option>
                              <option>Rejected</option>

                            </Input>
                        </td>
                        <td><Button  type="button"  className="text-primary shadow-none p-0" onClick={() => this.props.history.push('/admin/print')}><i className="fas fa-print"  aria-hidden="true"></i> Print</Button></td>
                        <td><Button  type="button" onClick={this.toggleModal} className="text-success shadow-none p-0"><i class="fas fa-eye" aria-hidden="true"></i> View</Button></td>
                      </tr>
                      
                    </tbody>
                  </Table>
                </CardBody>
                <div className="text-center mt-3 mb-3">
                  <Button className="bg-red border-0" type="button">
                    Submit
                  </Button>
                </div>
              </Card>
            </Col>
          </Row>
          {/* View modalbox */}
          <Modal className="modal-size"  isOpen={this.state.isOpen}>
            <ModalHeader className="bg-light-gray" toggle={this.toggleModal}>
              <h3 className="mb-0 text-16">
                Report
              </h3>
            </ModalHeader>
            <ModalBody className="pt-0">
            <Row>
              <div className="col">
                <Form role="form" className="mt-3 Custom-form">
                  <Row>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Refrence ID 
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="809828909" type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Submission Date 
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="20-07-2020" type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Amount 
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="3000" type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Approved By
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="Abha Varma" type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Approved Attachemnt
                        </Label>
                        <a href="#test" className="text-danger"> attachment-of-the-bill.pdf </a>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Invoice Attachemnt
                        </Label>
                        <a href="#test" className="text-danger"> attachment-of-the-invoice.docx </a>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="12">
                      <FormGroup>
                        <Label>
                          Description
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="testing process" type="textarea" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Payment Statue
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="Pending" type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Recjected Reason
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="Test Data" type="text" autoComplete=""/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label for="PaymentStatus">Status</Label>
                        <Input type="select" name="select" id="PaymentStatus">
                          <option> Pending </option>
                          <option> On Hold</option>
                          <option> In Process</option>
                          <option> Paid</option>
                          <option>Processed</option>
                          <option>Rejected</option>
                          <option>Document Received</option>

                        </Input>
                      </FormGroup>
                    </Col>
                    {/* Finance Edit Section */}
                    <Col className="" lg="12">
                    <h4 className="text-primary">Finance Section</h4>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label for="PaymentStatus">Change Status</Label>
                        <Input type="select" name="select" id="PaymentStatus">
                          <option> Rejected </option>
                          <option> On Hold</option>
                          <option> In Process</option>
                          <option> Paid</option>
                          <option>Processed</option>
                          <option>Pending</option>
                          <option>Document Received</option>

                        </Input>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="8">
                      <FormGroup>
                        <Label>
                        Reject / On Hold Reason
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="Reject / On Hold Reason" type="textarea" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
              </div>
            </Row>
              <Row>
                <Col className="text-center mt-4 pt-1" lg="12">
                  <Button className="bg-red border-0" type="button">
                    Confirm
                  </Button>
                  <Button className="bg-light-gray border-0" type="button">
                    Cancel
                  </Button>
                </Col>
              </Row> 
            </ModalBody>
          </Modal>
        </Container>
      </>
    );
  }
}

export default Approver;
