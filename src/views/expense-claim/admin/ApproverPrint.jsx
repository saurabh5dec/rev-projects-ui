import React from "react";

// reactstrap components
import { Button, Container, Table, Col, Row, Card, CardBody } from "reactstrap";

class ApproverPrint extends React.Component {
  render() {
    return (
      <>
        {/* Detail */}
        <div className=" pb-2 pt-5 pt-md-5">
          <Container fluid>
            <div className="">
              {/* Card stats */}
              <Row>
                <Col lg="12" xl="12">
                  <Card className="card-stats mb-4 mb-xl-0">
                    <CardBody>
                      <Row>
                        <Col className="Main-section">
                          <h1 className="text-left d-inline">Expense Claim Form</h1>
                          <div className="float-right d-inline">
                            <Button className="bg-red border-0" type="button">
                              Print
                            </Button>
                          </div>
                          <Table className="Emp-details mt-4 mb-5">
                            <tr className="bg-light-gray">
                              <td>
                                <b> Payment request number :</b> 44804
                              </td>
                              <td>
                                <b>Payment request date :</b> 02-09-2018
                              </td>
                              <td>
                                <b>Payment request type :</b> Reimbursement
                              </td>
                            </tr>
                          </Table>
                          <Col>
                            <h2> Dear Peter Dinklage</h2>
                            <p>
                              Your rail reservation request has been
                              successfully submitted as per details:
                            </p>
                          </Col>
                          <Table className="Emp-details mt-5">
                            <tr>
                              <td>
                                <b className="mr-5">Request Type :</b> <span style={{float : "right"}}>Test</span>
                              </td>
                              <td>
                                <b className="mr-5 ml-5">Amount :</b> <span style={{float : "right"}}>Test</span>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <b className="mr-5">Bank Name :</b> <span style={{float : "right"}}>Test</span>
                              </td>
                              <td>
                                <b className="mr-5 ml-5">Account Number :</b> <span style={{float : "right"}}>Test</span>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <b className="mr-5">IFSC :</b> <span style={{float : "right"}}>Test</span>
                              </td>
                              <td>
                                <b className="mr-5 ml-5">Approved By :</b> <span style={{float : "right"}}>Test</span>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <b className="mr-5">Description :</b> <span style={{float : "right"}}>Test</span>
                              </td>
                            </tr>
                          </Table>
                          <h3 className="mt-5"> 	Requester Details :- </h3>
                          <Table className="Emp-details mt-4 mb-5">
                            <tr className="bg-light-gray">
                              <td>
                                <b> Name :</b> 	Saurabh Misra
                              </td>
                              <td>
                                <b>EmployeeId :</b> FLS/IND/0055
                              </td>
                              <td>
                                <b>Location :</b> 6748-HO-IT
                              </td>
                            </tr>
                          </Table>
                          <Col>
                          <b style={{float : "right"}}>Signature</b>
                          <br></br>
                          <br></br>
                          <br></br>
                          <br></br>
                          </Col>
                          <p>Please attach the supporting document and submit to local cashier for sending to FCM Gurgaon Office for payment.</p>
                        </Col>
                        
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </div>
          </Container>
        </div>
        <Container className="mt-4" fluid></Container>
      </>
    );
  }
}

export default ApproverPrint;
