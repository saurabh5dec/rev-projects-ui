 
import React from "react";
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
// reactstrap components
import {
  Button,
  Card,
  CardTitle,
  CardBody,
  CardHeader,
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Badge,
  Label,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  InputGroup,
 
} from "reactstrap";
import moment from 'moment';
import ReactPaginate from 'react-paginate';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import { formatDate, parseDate } from 'react-day-picker/moment';
import ExpenseService from "service/ExpenseService";

class Approver extends React.Component {
  constructor(props) {
    super(props);
    this.handleFromChange = this.handleFromChange.bind(this);
    this.handleToChange = this.handleToChange.bind(this);
    this.findAllPendingRequest=this.findAllPendingRequest.bind(this)
    this.UniqueCodeHandle =this.UniqueCodeHandle.bind(this)
    this.handleDropdownChange = this.handleDropdownChange.bind(this)
    this.handleEmployeeName = this.handleEmployeeName.bind(this)
    this.toggleModal= this.toggleModal.bind(this);
    this.handleExpenseDropdownChange =this.handleExpenseDropdownChange.bind(this)
    this.handleChangeRejectStatus=this.handleChangeRejectStatus.bind(this)
    
  this.toggle = this.toggle.bind(this);
  this.print = this.print.bind(this);

    this.state = {
      from: undefined,
      to: undefined,
      requestFormData:[],
      uniqueCode:'',
      status:'',
      name:'',
      isOpen: false,
      amount:'',
      approvedBy:'',
      fileName:'',
      invFileName:'',
      api:'',
      api1:'',
      offset: 0,
      perPage: 10,
      currentPage: 0,
      tableData:[],
      selectValue:'Pending',
      rejectedStatus:'',
      modal: false,
      countList:[],
      description:'',
      paymentStatus:'',
      rejectedReason:'',
    };
  }




print =() => {
  var content = document.getElementById('printarea');
  var pri = document.getElementById('ifmcontentstoprint').contentWindow;
  pri.document.open();
  pri.document.write(content.innerHTML);
  pri.document.close();
  pri.focus();
  pri.print();
}

// renderContent() {
//   var i = 0;
//   debugger
//   return this.state.tableData.map((d) => {
//       return (<p key={d + i++}>{i} - {d}</p>)
//   });  
// }

toggle = (requestFormData) => {
  
  this.setState({
      modal: !this.state.modal,
      uniqueCode:requestFormData.uniqueCode,
    amount:requestFormData.amount,
    approvedBy:requestFormData.approvedBy,
    fileName:requestFormData.fileName,
    invFileName:requestFormData.invFileName,
    description:requestFormData.description,
  })
}











//this function is used to select the location from dropdown 
handleExpenseDropdownChange(e){
 
  this.setState({ selectValue: e.target.value });
}


handleChangeRejectStatus(e){
 
  // this.setState({ rejectedStatus: e.target.value });
  this.setState({rejectedReason: e.target.value });
}



toggleConfirmationModal = () => {
  this.setState({ isOpen: !this.state.isOpen });
};

  showFromMonth() {
    const { from, to } = this.state;
    if (!from) {
      return;
    }
    if (moment(to).diff(moment(from), 'months') < 2) {
      this.to.getDayPicker().showMonth(from);
    }
  }

  handleFromChange(from) {
    // Change the from date and focus the "to" input field
    this.setState({ from });
  }

  handleToChange(to) {
    this.setState({ to }, this.showFromMonth);
  }


toggleModal = (requestFormData) => {
 let url= 'http://localhost:8094/api/employee/download/?fileName='+requestFormData.invFileName;
 let url1= 'http://localhost:8094/api/employee/download/?fileName='+requestFormData.fileName;
  this.setState({ 
    isOpen: !this.state.isOpen,
    uniqueCode:requestFormData.uniqueCode,
    amount:requestFormData.amount,
    approvedBy:requestFormData.approvedBy,
    fileName:requestFormData.fileName,
    invFileName:requestFormData.invFileName,
    api:url,
    api1:url1,
    status:requestFormData.status
  });
};


componentDidMount() {
  this.findAllPendingRequest();
}



findAllPendingRequest(){
  const user = JSON.parse(localStorage.getItem('user'));
    ExpenseService.findAllApplyPendingData(user.id)
        .then((res) => {
          var quantity=[];
          // for (const [index, value] of res.data.entries()) {
          // //  alert(value.status)
          //   quantity.push(value.status)
          // }
          var data = res.data;
          var slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)
          this.setState({
            requestFormData:res.data,
            pageCount: Math.ceil(data.length / this.state.perPage),
            tableData:slice,
           // countList:quantity,
          })
      });
          
}


handlePageClick = (e) =>{
  const selectedPage = e.selected;
  const offset= selectedPage * this.state.perPage;
  this.setState({
    currentPage:selectedPage,
    offset:offset
  }, () =>{
    this.loadMoreData();
   });
}


loadMoreData(){
  const data=this.state.requestFormData;
  const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)
  this.setState({
    pageCount: Math.ceil(data.length / this.state.perPage),
    tableData:slice,
  });
}












UniqueCodeHandle =(e)=>{
  this.setState({
    uniqueCode:e.target.value,
  });
}

//this function is used to select the location from dropdown 
handleDropdownChange(e) {
  this.setState({ status: e.target.value });
}

handleEmployeeName =(e)=>{
  this.setState({
    name:e.target.value
  })
}


searchData =(e) =>{
  ExpenseService.findData(this.state.from,this.state.to,this.state.uniqueCode,
    this.state.status,this.state.name)
  .then((res) => {
    this.setState({
      requestFormData:res.data,
    });
  });
}

updateStatus =(e) =>{
  const RequestForm={status:this.state.selectValue,rejectedStatus:this.state.rejectedStatus,uniqueCode:this.state.uniqueCode}

  ExpenseService.updateStatus(RequestForm)
  .then((res) => {
    this.setState({
      //requestFormData:res.data,
    });
  });
}

updateFinanceStatus =(e) =>{

  const RequestForm={paymentStatus:this.state.selectValue,rejectedReason:this.state.rejectedReason,paymentStatus:this.state.selectValue,uniqueCode:this.state.uniqueCode}
  ExpenseService.updateFinanceStatus(RequestForm)
  .then((res) => {
    this.setState({
      //requestFormData:res.data,
    });
  });
}


//this function is used to increment the quantity
updatePendingStatus(newValue,index) {
  //copy the array first
 const updatedArray = [...this.state.countList];
     updatedArray[index] = newValue.target.value;
    this.setState({
        countList: updatedArray,
     });
  
}



applyProduct = () => {
  debugger
  const user = JSON.parse(localStorage.getItem('user'));
  const credentials = {countList:this.state.countList,userId:user.id}
  ExpenseService.applyRequest(credentials)
  .then((res) => {
    this.findAllPendingRequest();
  });
}






  render() {
    const { from, to } = this.state;
    const modifiers = { start: from, end: to };
    return (
      <>
       {/* Detail */}
       <div className=" pb-2 pt-5 pt-md-5">
          <Container fluid>
            <div className="">
              {/* Card stats */}
              <Row>
                <Col lg="6" xl="6">
                  <Card className="card-stats mb-4 mb-xl-0">
                    <CardBody>
                      <Row>
                        <div className="col">
                          <CardTitle
                            tag="h5"
                            className="text-uppercase text-muted mb-0"
                          >
                          Status
                          </CardTitle>
                          <span className="h5 font-weight-bold mb-0">
                          Pending List: <span>30 Queries</span>
                          </span>
                          <br></br>
                          <span className="h5 font-weight-bold mb-0">
                           On Hold : <span>5 Queries</span>
                          </span>
                        </div>
                        <Col className="col-auto">
                          <div className="icon icon-shape bg-yellow text-white rounded-circle shadow">
                            <i className="ni ni-check-bold" />
                          </div>
                        </Col>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
                <Col lg="6" xl="6">
                  <Card className="card-stats mb-4 mb-xl-0">
                    <CardBody>
                      <Row>
                        <div className="col">
                          <CardTitle
                            tag="h5"
                            className="text-uppercase text-muted mb-0"
                          >
                            Approved Details
                          </CardTitle>
                          <span className="h5 font-weight-bold mb-0">
                            Approved List : <span>50 Queries</span>
                          </span>
                          <br></br>
                          <span className="h5 font-weight-bold mb-0">
                           Rejected  : <span>10 Queries</span>
                          </span>
                        </div>
                        <Col className="col-auto">
                          <div className="icon icon-shape bg-success text-white rounded-circle shadow">
                            <i className="ni ni-check-bold" />
                          </div>
                        </Col>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </div>
          </Container>
        </div> 
        {/* Search Form */}
       <div className="header pb-3 pt-4 pt-md-4">
          <Container fluid>
            <div className="header-body">
              {/* Card stats */}
              <Row>
                  <Col lg="12" xl="12">
                    <Card className="card-stats mb-4 mb-xl-0">
                      <CardBody>
                        <Row>
                          <div className="col">
                             <h3 className="mb-0">Search Detail</h3>
                            <Form role="form" className="mt-3 Custom-form">
                              <Row>
                              <Col className="Subfrom-to" lg="12">
                                  <Row>
                                    <Col className="" lg="6">
                                      <FormGroup>
                                        <Label for="FromDate">Submission Date From</Label><br></br>
                                        <DayPickerInput type="date"
                                        value={from}
                                        placeholder="From"
                                        format="LL"
                                        formatDate={formatDate}
                                        parseDate={parseDate}
                                        dayPickerProps={{
                                          selectedDays: [from, { from, to }],
                                          disabledDays: { after: to },
                                          toMonth: to,
                                          modifiers,
                                          numberOfMonths: 2,
                                          onDayClick: () => this.to.getInput().focus(),
                                        }}
                                        onDayChange={this.handleFromChange}
                                      />
                                      </FormGroup>
                                    </Col>
                                    <Col className="" lg="6">
                                      <FormGroup>
                                        <Label for="ToDate">To</Label><br></br>
                                        <DayPickerInput type="text" 
                                          ref={el => (this.to = el)}
                                          value={to}
                                          placeholder="To"
                                          format="LL"
                                          formatDate={formatDate}
                                          parseDate={parseDate}
                                          dayPickerProps={{
                                            selectedDays: [from, { from, to }],
                                            disabledDays: { before: from },
                                            modifiers,
                                            month: from,
                                            fromMonth: from,
                                            numberOfMonths: 2,
                                          }}
                                          onDayChange={this.handleToChange}
                                        />
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                </Col>
                                <Col className="" lg="4">
                                    <FormGroup>
                                      <Label for="referenceid">System ID Number</Label>
                                      <Input
                                        type="number"
                                        name="referenceid"
                                        placeholder="System ID Number"
                                        onChange={this.UniqueCodeHandle}
                                      />
                                    </FormGroup>
                                  </Col>
                                <Col className="" lg="4">
                                  <FormGroup>
                                    <Label for="PaymentStatus">Status</Label>
                                    <Input type="select" name="select" onChange={this.handleDropdownChange} id="PaymentStatus">
                                      <option> Pending </option>
                                      <option> On Hold</option>
                                      <option> In Process</option>
                                      <option> Paid</option>
                                      <option>Processed</option>
                                      <option>Rejected</option>
                                      <option>Document Received</option>

                                    </Input>
                                  </FormGroup>
                                </Col>
                                <Col className="" lg="4">
                                    <FormGroup>
                                      <Label for="Employeename">Employee Name </Label>
                                      <Input
                                        type="text"
                                        name="Employeename"
                                        placeholder="Employee Name"
                                        onChange={this.handleEmployeeName}
                                      />
                                    </FormGroup>
                                  </Col>
                              </Row>
                              <div className="text-center mt-3">
                                <Button onClick={this.searchData} className="bg-red border-0" type="button">
                                  Search
                                </Button>
                              </div>
                            </Form>
                          </div>
                        </Row>
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
              </div>
           </Container>
        </div>
      {/* Page content */}
      
        <Container className="mt-3" fluid>
          <Row className="">
            <Col className="mb-5 mb-xl-0" xl="12">
              <Card className="shadow">
                <CardHeader className="border-0 pb-0">
                  <Row className="align-items-center">
                    <div className="col">
                      <h3 className="mb-0">Tracking Report</h3>
                    </div>
                    {/* <div className="text-center mt-3">
                      <Button className="bg-red border-0" type="button">
                        <i className="fas fa-download mr-2"></i>
                        Export Excel
                      </Button>
                    </div> */}
                    <div className="text-center mt-3">  
                                        <ReactHTMLTableToExcel  
                                                className="btn btn-info"  
                                                table="emp"  
                                                filename="ReportExcel"  
                                                sheet="Sheet"  
                                                buttonText="Export excel" />  
                                </div>  
                  </Row>
                </CardHeader>
                <CardBody>
                  <Table id="emp" className="align-items-center table-flush" responsive>
                    <thead className="thead-light">
                      <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Sub Date</th>
                        <th scope="col">Name</th>
                        <th scope="col">Branch</th>
                        <th scope="col">Emp. ID</th>
                        <th scope="col">Amount (INR)</th>
                        <th scope="col">Approved By</th>
                        <th scope="col">Approved Attachemnt</th>
                        <th scope="col">Invoice Attachemnt</th>
                        <th scope="col">Finance Status</th>
                        <th scope="col">Approver Status</th>
                        <th scope="col">Print</th>
                        <th scope="col">Detail</th>
                      </tr>
                    </thead>
                    <tbody>
                      
                      { this.state.tableData.map((requestFormData,index) => 
                    <tr key={requestFormData.requestId}>
                       <td>{requestFormData.requestId}</td>
                       <td>{requestFormData.fromDate}</td>
                       <td>{requestFormData.name}</td>
                       <td>{}</td>
                       <td>{requestFormData.employeeId}</td>
                       <td>{requestFormData.amount}</td>
                      <td>{requestFormData.approvedBy}</td>
                      <td>{requestFormData.fileName}</td>
                       <td>{requestFormData.invFileName}</td>
                        <td><Badge color="" className="badge-dot mr-4">
                          <i className="bg-warning" />
                          {requestFormData.status}
                        </Badge></td>
                        <td>
                            <Input type="select"  onChange={e => this.updatePendingStatus(e,index)} className="p-1" name="select" id={this.state.countList[index]} >
                              <option> Pending </option> 
                              <option > On Hold</option>
                              <option> In Process</option>
                              <option> Paid</option>
                              <option>Processed</option>
                              <option>Rejected</option>

                            </Input>
                        </td> 
                        <td><Button  type="button" onClick={(e) => this.toggleModal(requestFormData)}  className="text-primary shadow-none p-0"><i className="fas fa-print"  aria-hidden="true"></i> Print</Button></td>
                        <td><Button  type="button" onClick={(e)=>this.toggle(requestFormData)} className="text-success shadow-none p-0"><i class="fas fa-eye" aria-hidden="true"></i> View</Button></td>
                      </tr>
                        )}
                      {/* <tr>
                        <th scope="row">2</th>
                        <td>12-08-2020</td>
                        <td>Saurabh Misra</td>
                        <td>Noida HO</td>
                        <td>FCM/IND/XXXX</td>
                        <td>2000</td>
                        <td>Abha Varma</td>
                        <td>attachment</td>
                        <td>attachment1</td>
                        <td><Badge color="" className="badge-dot mr-4">
                          <i className="bg-warning" />
                          pending
                        </Badge></td>
                        <td>
                            <Input type="select" className="p-1" name="select" id="ApproverStatus">
                              <option> Pending </option>
                              <option> On Hold</option>
                              <option> In Process</option>
                              <option> Paid</option>
                              <option>Processed</option>
                              <option>Rejected</option>

                            </Input>
                        </td>
                        <td><Button  type="button"  className="text-primary shadow-none p-0"><i className="fas fa-print"  aria-hidden="true"></i> Print</Button></td>
                        <td><Button  type="button" onClick={this.toggleModal} className="text-success shadow-none p-0"><i class="fas fa-eye" aria-hidden="true"></i> View</Button></td>
                      </tr> */}
                      
                    </tbody>
                  </Table>
                  <ReactPaginate
                    previousLabel={"prev"}
                    nextLabel={"next"}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={this.state.pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={this.handlePageClick}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"}/>
                </CardBody>
                <div className="text-center mt-3 mb-3">
                  <Button onClick={this.applyProduct} className="bg-red border-0" type="button">
                    Submit
                  </Button>
                </div>
              </Card>
            </Col>
          </Row>
          {/* View modalbox */}
          <Modal className="modal-size"  isOpen={this.state.isOpen}>
            <ModalHeader className="bg-light-gray" toggle={this.toggleModal}>
              <h3 className="mb-0 text-16">
                Print Report
              </h3>
            </ModalHeader>
            <ModalBody className="pt-0">
            <Row>
              <div className="col">
                <Form role="form" className="mt-3 Custom-form">
                  <Row>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Refrence ID 
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="809828909" value={this.state.uniqueCode} type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Submission Date 
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="20-07-2020" type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Amount 
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="3000" value={this.state.amount} type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Approved By
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="Abha Varma" value={this.state.approvedBy} type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Approved Attachemnt
                        </Label>
                        <a href={this.state.api1} className="text-danger">{this.state.fileName} </a>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Invoice Attachemnt
                        </Label>
                        <a href={this.state.api} className="text-danger"> {this.state.invFileName} </a>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="12">
                      <FormGroup>
                        <Label>
                          Description
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="testing process" type="textarea" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Payment Statue
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="Pending" value={this.state.status} type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Recjected Reason
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="Test Data"  onChange={this.handleChangeRejectStatus} type="text" autoComplete=""/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label for="PaymentStatus">Status</Label>
                        <Input type="select"   onChange={this.handleExpenseDropdownChange} name="select" id="PaymentStatus">
                          <option> Pending </option>
                          <option> On Hold</option>
                          <option> In Process</option>
                          <option> Paid</option>
                          <option>Processed</option>
                          <option>Rejected</option>
                          <option>Document Received</option>

                        </Input>
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
              </div>
            </Row>
              <Row>
                <Col className="text-center mt-4 pt-1" lg="12">
                  <Button onClick={this.updateStatus} className="bg-red border-0" type="button">
                    Confirm
                  </Button>
                  <Button className="bg-light-gray border-0" type="button">
                    Cancel
                  </Button>
                </Col>
              </Row> 
            </ModalBody>
          </Modal>


       {/* View modalbox */}
             <div>
              <Modal 
                    size='lg' 
                    isOpen={this.state.modal} 
                    toggle={this.toggle} 
                    className='results-modal'
                >  
                    <ModalHeader className="bg-light-gray" toggle={this.toggle}>
                    <h3 className="mb-0 text-16">
                        View Report
                        </h3>
                    </ModalHeader>
                    <iframe id="ifmcontentstoprint" style={{
                        height: '0px',
                        width: '0px',
                        position: 'absolute'
                    }}></iframe>      
                    {/* <Button onClick={this.print}>Print</Button> */}
             
           <ModalBody id='printarea' className="pt-0">
             <Row>
              <div className="col">
                <Form role="form" className="mt-3 Custom-form">
                  <Row>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Refrence ID 
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="809828909" value={this.state.uniqueCode} type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Submission Date 
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="20-07-2020" type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Amount 
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="3000" value={this.state.amount} type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Approved By
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="Abha Varma" value={this.state.approvedBy} type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Approved Attachemnt
                        </Label>
                        <a href={this.state.api1} className="text-danger">{this.state.fileName} </a>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Invoice Attachemnt
                        </Label>
                        <a href={this.state.api} className="text-danger"> {this.state.invFileName} </a>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="12">
                      <FormGroup>
                        <Label>
                          Description
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="testing process" value={this.state.description} type="textarea" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                  

                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Payment Status
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="Pending" type="text" autoComplete="" disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label>
                          Recjected Reason
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="Test Data" type="text" autoComplete=""/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label for="PaymentStatus">Status</Label>
                        <Input type="select" name="select" id="PaymentStatus">
                          <option> Pending </option>
                          <option> On Hold</option>
                          <option> In Process</option>
                          <option> Paid</option>
                          <option>Processed</option>
                          <option>Rejected</option>
                          <option>Document Received</option>

                        </Input>
                      </FormGroup>
                    </Col>
                    {/* Finance Edit Section */}
                    <Col className="" lg="12">
                    <h4 className="text-primary">Finance Section</h4>
                    </Col>
                    <Col className="" lg="4">
                      <FormGroup>
                        <Label for="PaymentStatus">Change Status</Label>
                        <Input type="select" onChange={this.handleExpenseDropdownChange}  name="select" id="PaymentStatus">
                          <option> Rejected </option>
                          <option> On Hold</option>
                          <option> In Process</option>
                          <option> Paid</option>
                          <option>Processed</option>
                          <option>Pending</option>
                          <option>Document Received</option>

                        </Input>
                      </FormGroup>
                    </Col>
                    <Col className="" lg="8">
                      <FormGroup>
                        <Label>
                        Reject / On Hold Reason
                        </Label>
                        <InputGroup className="input-group-alternative">
                          <Input placeholder="Reject / On Hold Reason" onChange={this.handleChangeRejectStatus} type="text"   autoComplete=""/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
               </div>
            </Row>
            <Row>
                <Col className="text-center mt-4 pt-1" lg="12">
                  <Button onClick={this.updateFinanceStatus} className="bg-red border-0" type="button">
                    Confirm
                  </Button>
                  <Button className="bg-light-gray border-0" type="button">
                    Cancel
                  </Button>
                </Col>
              </Row>
            </ModalBody>
          </Modal>
          </div>
        </Container>
      </>
    );
  }
}

export default Approver;
