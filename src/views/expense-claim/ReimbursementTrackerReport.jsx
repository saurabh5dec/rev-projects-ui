
import React from "react";

// reactstrap components
import {
  Button,
  Card,
  CardTitle,
  CardBody,
  CardHeader,
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Badge,
  Label,
  Table
 
} from "reactstrap";
import ExpenseService from "service/ExpenseService";
import ReactPaginate from 'react-paginate';
import moment from 'moment';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import { formatDate, parseDate } from 'react-day-picker/moment'

class ReimbursementTrackerReport extends React.Component {
  constructor(props) {
    super(props);
    this.handleFromChange = this.handleFromChange.bind(this);
    this.handleToChange = this.handleToChange.bind(this);
    this.fetchUserById=this.fetchUserById.bind(this)
    this.findAllPendingRequest=this.findAllPendingRequest.bind(this)
    this.UniqueCodeHandle =this.UniqueCodeHandle.bind(this)
    this.handleDropdownChange = this.handleDropdownChange.bind(this)
    this.handleEmployeeName = this.handleEmployeeName.bind(this)


    this.state = {
      from: undefined,
      to: undefined,
      userData:[],
      requestFormData:[],
      uniqueCode:'',
      status:'',
      name:'',
      offset: 0,
      perPage: 10,
      currentPage: 0,
      tableData:[]
    
    };
  }
  showFromMonth() {
    const { from, to } = this.state;
    if (!from) {
      return;
    }
    if (moment(to).diff(moment(from), 'months') < 2) {
      this.to.getDayPicker().showMonth(from);
    }
  }

  handleFromChange(from) {
    // Change the from date and focus the "to" input field
    this.setState({ from });
  }

  handleToChange(to) {
    this.setState({ to }, this.showFromMonth);
  }

  componentDidMount() {
    this.fetchUserById();
    this.findAllPendingRequest();
}

  fetchUserById(){
    const user = JSON.parse(localStorage.getItem('user'));
    ExpenseService.fetchUser(user.id)
        .then((res) => {
          this.setState({
            userData:res.data,
          });
        });
  }

findAllPendingRequest(){
  const user = JSON.parse(localStorage.getItem('user'));
    ExpenseService.findAllApplyData(user.id)
        .then((res) => {
          var data = res.data;
          var slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)
          this.setState({
            requestFormData:res.data,
            pageCount: Math.ceil(data.length / this.state.perPage),
            tableData:slice,
          })
      });
          
}

UniqueCodeHandle =(e)=>{
  this.setState({
    uniqueCode:e.target.value,
  });
}

//this function is used to select the location from dropdown 
handleDropdownChange(e) {
  this.setState({ status: e.target.value });
}

handleEmployeeName =(e)=>{
  this.setState({
    name:e.target.value
  })
}


searchData =(e) =>{

  ExpenseService.findData(this.state.from,this.state.to,this.state.uniqueCode,
    this.state.status,this.state.name)
  .then((res) => {
    this.setState({
      requestFormData:res.data,
    });
  });
}





handlePageClick = (e) =>{
  const selectedPage = e.selected;
  const offset= selectedPage * this.state.perPage;
  this.setState({
    currentPage:selectedPage,
    offset:offset
  }, () =>{
    this.loadMoreData();
   });
}


loadMoreData(){
  const data=this.state.requestFormData;
  const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)
  this.setState({
    pageCount: Math.ceil(data.length / this.state.perPage),
    tableData:slice,
  });
}






  render() {
    const { from, to } = this.state;
    const modifiers = { start: from, end: to };
    return (
      <>
      {/* Employee Detail */}
      <div className=" pb-2 pt-5 pt-md-5">
          <Container fluid>
            <div className="">
              {/* Card stats */}
              <Row>
                <Col lg="6" xl="6">
                  <Card className="card-stats mb-4 mb-xl-0">
                    <CardBody>
                      <Row>
                        <div className="col">
                          <CardTitle
                            tag="h5"
                            className="text-uppercase text-muted mb-0"
                          >
                            Approver Details
                          </CardTitle>
                          <span className="h5 font-weight-bold mb-0">
                          Approver Name: <span>{this.state.userData.approvedBy}</span>
                          </span>
                          <br></br>
                          <span className="h5 font-weight-bold mb-0">
                           Email : <span>{this.state.userData.approvedEmail}</span>
                          </span>
                        </div>  
                        <Col className="col-auto">
                          <div className="icon icon-shape bg-warning text-white rounded-circle shadow">
                            <i className="fas fa-user" />
                          </div>
                        </Col>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
                <Col lg="6" xl="6">
                  <Card className="card-stats mb-4 mb-xl-0">
                    <CardBody>
                      <Row>
                        <div className="col">
                          <CardTitle
                            tag="h5"
                            className="text-uppercase text-muted mb-0"
                          >
                            Employee Details
                          </CardTitle>
                          <span className="h5 font-weight-bold mb-0">
                            Branch : <span>Noida</span>
                          </span>
                          <br></br>
                          <span className="h5 font-weight-bold mb-0">
                           Employee ID : <span>{this.state.userData.employeeId}</span>
                          </span>
                        </div>
                        <Col className="col-auto">
                          <div className="icon icon-shape bg-info text-white rounded-circle shadow">
                            <i className="fas fa-user" />
                          </div>
                        </Col>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </div>
          </Container>
        </div> 
      {/* Search Form */}
        <div className="header pb-3 pt-4 pt-md-4">
          <Container fluid>
            <div className="header-body">
              {/* Card stats */}
              <Row>
                  <Col lg="12" xl="12">
                    <Card className="card-stats mb-4 mb-xl-0">
                      <CardBody>
                        <Row>
                          <div className="col">
                             <h3 className="mb-0">Details</h3>
                            <Form role="form" className="mt-3 Custom-form">
                              <Row>
                                <Col className="Subfrom-to" lg="12">
                                  <Row>
                                    <Col className="" lg="6">
                                      <FormGroup>
                                        <Label for="FromDate">Submission Date From</Label><br></br>
                                        <DayPickerInput type="date"
                                        value={from}
                                        placeholder="From"
                                        format="LL"
                                        formatDate={formatDate}
                                        parseDate={parseDate}
                                        dayPickerProps={{
                                          selectedDays: [from, { from, to }],
                                          disabledDays: { after: to },
                                          toMonth: to,
                                          modifiers,
                                          numberOfMonths: 2,
                                          onDayClick: () => this.to.getInput().focus(),
                                        }}
                                        onDayChange={this.handleFromChange}
                                      />
                                      </FormGroup>
                                    </Col>
                                    <Col className="" lg="6">
                                      <FormGroup>
                                        <Label for="ToDate">To</Label><br></br>
                                        <DayPickerInput type="text" 
                                          ref={el => (this.to = el)}
                                          value={to}
                                          placeholder="To"
                                          format="LL"
                                          formatDate={formatDate}
                                          parseDate={parseDate}
                                          dayPickerProps={{
                                            selectedDays: [from, { from, to }],
                                            disabledDays: { before: from },
                                            modifiers,
                                            month: from,
                                            fromMonth: from,
                                            numberOfMonths: 2,
                                          }}
                                          onDayChange={this.handleToChange}
                                        />
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                </Col>
                                <Col className="" lg="4">
                                    <FormGroup>
                                      <Label for="referenceid">Payment reference ID</Label>
                                      <Input
                                        type="number"
                                        name="referenceid"
                                        placeholder="Payment reference ID"
                                        onChange={this.UniqueCodeHandle}
                                      />
                                    </FormGroup>
                                  </Col>
                                <Col className="" lg="4">
                                  <FormGroup>
                                    <Label for="PaymentStatus">Payment Status</Label>
                                    <Input type="select" onChange={this.handleDropdownChange} name="select" id="PaymentStatus">
                                      <option> Pending </option>
                                      <option> On Hold</option>
                                      <option> In Process</option>
                                      <option> Paid</option>
                                      <option>Processed</option>
                                      <option>Rejected</option>
                                      <option>Document Received</option>

                                    </Input>
                                  </FormGroup>
                                </Col>
                                <Col className="" lg="4">
                                    <FormGroup>
                                      <Label for="Employeename">Employee Name </Label>
                                      <Input
                                        type="text"
                                        name="Employeename"
                                        placeholder="Employee Name"
                                        onChange={this.handleEmployeeName}
                                      />
                                    </FormGroup>
                                  </Col>
                              </Row>
                              <div className="text-center mt-3">
                                <Button onClick={this.searchData} className="bg-red border-0" type="button">
                                  Search
                                </Button>
                              </div>
                            </Form>
                          </div>
                        </Row>
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
              </div>
           </Container>
        </div>
      {/* Page content */}
      
        <Container className="mt-3" fluid>
          <Row className="">
            <Col className="mb-5 mb-xl-0" xl="12">
              <Card className="shadow">
                <CardHeader className="border-0 pb-0">
                  <Row className="align-items-center">
                    <div className="col">
                      <h3 className="mb-0"></h3>
                    </div>
                    <div className="text-center mt-3">
                      <Button className="bg-red border-0" type="button">
                        <i className="fas fa-download mr-2"></i>
                        Export Excel
                      </Button>
                      <Button className="bg-red border-0" type="button">
                        <i className="fas fa-download mr-2"></i>
                        Export Master Excel
                      </Button>
                    </div>
                  </Row>
                </CardHeader>
                <CardBody>
                  <Table className="align-items-center table-flush" responsive>
                    <thead className="thead-light">
                      <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Request Type</th>
                        <th scope="col">Sub Date</th>
                        <th scope="col">Name</th>
                        <th scope="col">Amount (INR)</th>
                        <th scope="col">Nature Of Expense</th>
                        <th scope="col">Approved By</th>
                        <th scope="col">Doc Received Date</th>
                        <th scope="col">Doc Received</th>
                        <th scope="col">Finance Status</th>
                        <th scope="col">Approver Status</th>
                        <th scope="col">Approver Date</th>
                      </tr>
                    </thead>
                    <tbody>
                    { this.state.tableData.map((requestFormData,index) => 
                    <tr key={requestFormData.requestId}>
                       <td>{requestFormData.requestId}</td>
                       <td>{}</td>
                       <td>{requestFormData.fromDate}</td>
                       <td>{requestFormData.name}</td>
                       <td>{requestFormData.amount}</td>
                       <td>{}</td>
                      <td>{requestFormData.approvedBy}</td>

                      </tr>
                    )}




                      {/* <tr>
                        <th scope="row">1</th>
                        <td>Bill</td>
                        <td>12-08-2020</td>
                        <td>Saurabh Misra</td>
                        <td>2000</td>
                        <td>Travel for Meeting</td>
                        <td>Abha Varma</td>
                        <td>12-08-2020</td>
                        <td>Bill Recipt</td>
                        <td><Badge color="" className="badge-dot mr-4">
                          <i className="bg-warning" />
                          pending
                        </Badge></td>
                        <td><Badge color="" className="badge-dot mr-4">
                          <i className="bg-success" />
                          Approved
                        </Badge></td>
                        <td>16-08-2020</td>
                      </tr> */}
                     
                    </tbody>
                  </Table>
                  <ReactPaginate
                    previousLabel={"prev"}
                    nextLabel={"next"}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={this.state.pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={this.handlePageClick}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"}/>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

export default ReimbursementTrackerReport;
