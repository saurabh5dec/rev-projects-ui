
import React from "react";
import { Link } from "react-router-dom";
// reactstrap components
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  FormText, 
  InputGroup,
  Modal,
  ModalHeader,
  ModalBody,
  Label
 
} from "reactstrap";
import ExpenseService from "service/ExpenseService";


class ReimbursementForm extends React.Component {
  constructor(props) {
    super(props);
  // confirmation Popup
  this.state = {
    isOpen: false,
    isActive: false,
    employeeDetails:[],
    userData:[],
    bankName:null,
    nameInBank:'',
    accountNo:'',
    ifscCode:'',
    employeeName:'',
    employeeId:'',
    amount:'',
    selectValue:'',
    fromDate:'',
    toDate:'',
    selectedFile:'',
    selectedFile2:'',
    uniqueCode:'',
  };
  this.reloadEmployeeById=this.reloadEmployeeById.bind(this)
  this.fetchUserById=this.fetchUserById.bind(this)
  this.handleBankIfscCode = this.handleBankIfscCode.bind(this)
  this.handleExpenseDropdownChange =this.handleExpenseDropdownChange.bind(this)
  this.handleBillPeriodFrom =this.handleBillPeriodFrom.bind(this)
  this.handleBillPeriodTo =this.handleBillPeriodTo.bind(this)
  this.handleAmount=this.handleAmount.bind(this)
  this.onFileChangeHandler=this.onFileChangeHandler.bind(this)
  this.onFileChangeHandler2=this.onFileChangeHandler2.bind(this)
}

  
  componentDidMount() {
    this.reloadEmployeeById();
    this.fetchUserById();
}


reloadEmployeeById() {
  const user = JSON.parse(localStorage.getItem('user'));
  ExpenseService.fetchEmployeeDetails(user.id)
      .then((res) => {
        this.setState({
          employeeDetails:res.data,
        });
        
      });

}

fetchUserById(){
  const user = JSON.parse(localStorage.getItem('user'));
  ExpenseService.fetchUser(user.id)
      .then((res) => {
        this.setState({
          userData:res.data,
        });
      });
}

handleAmount(e){
  this.setState({ amount: e.target.value });

}

//this function is used to select the location from dropdown 
handleExpenseDropdownChange(e){
  this.setState({ selectValue: e.target.value });
}
handleBillPeriodFrom(e){
  this.setState({ fromDate: e.target.value });
  
}
handleBillPeriodTo(e){
  this.setState({ toDate: e.target.value });

}
  toggleModal = () => {
    this.setState({ isOpen: !this.state.isOpen });
  };

 //Thanks Popup   
  toggleThanksModal = () => {
    this.setState({ isActive: !this.state.isActive });
  }

  handleBankName = event => {
    this.setState({ bankName: event.target.value,
    });
  }

  handleBankAccountHolderName = event => {
    this.setState({ nameInBank: event.target.value,
    });
  }

  handleBankIfscCode = event => {
    this.setState({ ifscCode: event.target.value,
    });
  }
  
  handleBankAccountNo = event => {
    this.setState({ accountNo: event.target.value,
    });
  }
  updateDetails = (e) => {
    const user = JSON.parse(localStorage.getItem('user'));
    const BankDetail={bankName:this.state.bankName,nameInBank:this.state.nameInBank,accountNo:this.state.accountNo,
    ifscCode:this.state.ifscCode}
    ExpenseService.updateBankDetail(user.id,BankDetail)
      .then((res) => {
        this.setState({
          userData:res.data,
        });
        
      });
      
  }

  onFileChangeHandler = (e) => {
    this.setState({
        selectedFile:  e.target.files[0],
    });

  }


  onFileChangeHandler2 = (e) => {

    this.setState({
        selectedFile2:  e.target.files[0],
    });
  
  }





  applyExpence =(e) =>{
  
     const formData = new FormData() 
     formData.append('file', this.state.selectedFile)
     formData.append('file', this.state.selectedFile2)
    const user = JSON.parse(localStorage.getItem('user'));
      ExpenseService.upload(user.id,this.state.selectValue,this.state.amount,this.state.fromDate,this.state.toDate,formData)
      .then((res) => {
        this.toggleThanksModal()
        this.setState({
          uniqueCode:res.data,
        });
        
      });
  }







  render() {
    return (
      <>
       <div className="header pb-8 pt-3 pt-md-6">
          <Container fluid>
            <div className="header-body">
              {/* Card stats */}
              <Row>
                  <Col lg="12" xl="12">
                    <Card className="card-stats mb-4 mb-xl-0">
                      <CardBody>
                        <Row>
                          <div className="col">
                             <h3 className="mb-0">Employee Details</h3>
                            <Form role="form" className="mt-3 Custom-form">
                              <Row>
                                <Col className="" lg="3">
                                  <FormGroup className="mb-3">
                                    <Label>
                                      Employee Name
                                    </Label>
                                    <InputGroup className="input-group-alternative">
                                      <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                          <i className="ni ni-circle-08"  />
                                        </InputGroupText>
                                      </InputGroupAddon>
                                      <Input placeholder="" value={this.state.userData.employeeName} type="text" autoComplete="" disabled/>
                                    </InputGroup>
                                  </FormGroup>
                                </Col>
                                <Col className="" lg="3">
                                  <FormGroup>
                                    <Label>
                                    Branch
                                    </Label>
                                    <InputGroup className="input-group-alternative">
                                      <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                          <i className="ni ni-paper-diploma" />
                                        </InputGroupText>
                                      </InputGroupAddon>
                                      <Input placeholder=""  type="text" autoComplete="" disabled/>
                                    </InputGroup>
                                  </FormGroup>
                                </Col>
                                <Col className="" lg="3">
                                  <FormGroup>
                                    <Label>
                                      Email
                                    </Label>
                                    <InputGroup className="input-group-alternative">
                                      <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                          <i className="ni ni-email-83" />
                                        </InputGroupText>
                                      </InputGroupAddon>
                                      <Input placeholder="" value={this.state.userData.email} type="email" autoComplete="" disabled/>
                                    </InputGroup>
                                  </FormGroup>
                                </Col>
                                <Col className="" lg="3">
                                  <FormGroup>
                                    <Label>
                                      Employee Id
                                    </Label>
                                    <InputGroup className="input-group-alternative">
                                      <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                          <i className="ni ni-badge" />
                                        </InputGroupText>
                                      </InputGroupAddon>
                                      <Input placeholder="" value={this.state.userData.employeeId} type="text" autoComplete="" disabled/>
                                    </InputGroup>
                                  </FormGroup>
                                </Col>
                              </Row>
                              <h3 className="mb-0">Other Details</h3>
                              <Row>
                                <Col className="" lg="4">
                                  <FormGroup className="mb-3">
                                    <Label>
                                    Name of Account Holder
                                    </Label>
                                    <InputGroup className="input-group-alternative">
                                      <Input placeholder="Name" defaultValue={this.state.employeeDetails.nameInBank} onChange={this.handleBankAccountHolderName} type="text" autoComplete=""/>
                                    </InputGroup>
                                  </FormGroup>
                                </Col>
                                <Col className="" lg="4">
                                  <FormGroup>
                                    <Label>
                                    Bank Name
                                    </Label>
                                    <InputGroup className="input-group-alternative">
                                      <Input  defaultValue={this.state.employeeDetails.bankName} type="text" onChange={this.handleBankName} autoComplete=""/>
                                    </InputGroup>
                                  </FormGroup>
                                </Col>
                                <Col className="" lg="4">
                                  <FormGroup>
                                    <Label>
                                    Account No.
                                    </Label>
                                    <InputGroup className="input-group-alternative">
                                      <Input  defaultValue={this.state.employeeDetails.accountNo} onChange={this.handleBankAccountNo} type="text" autoComplete=""/>
                                    </InputGroup>
                                  </FormGroup>
                                </Col>
                                <Col className="" lg="4">
                                  <FormGroup>
                                    <Label>
                                      IFSC Code
                                    </Label>
                                    <InputGroup className="input-group-alternative">
             <Input  type="text" 
                    className="form-control"
                    defaultValue={this.state.employeeDetails.ifscCode}
                    name="ifscCode"
                    placeholder="ifsc Code"
                    onChange={this.handleBankIfscCode}     autoComplete=""/>
                                    </InputGroup>
                                  </FormGroup>
                                </Col>
                                <Col className="" lg="4">
                                  <FormGroup>
                                    <Label>
                                      Approved By
                                    </Label>
                                    <InputGroup className="input-group-alternative">
                                      <Input placeholder="" value={this.state.userData.approvedBy} type="text" autoComplete=""/>
                                    </InputGroup>
                                  </FormGroup>
                                </Col>
                                <Col className="" lg="4">
                                  <FormGroup>
                                    <Label>
                                      Approved Email
                                    </Label>
                                    <InputGroup className="input-group-alternative">
                                      <Input placeholder="" value={this.state.userData.approvedEmail} type="email" autoComplete=""/>
                                    </InputGroup>
                                  </FormGroup>
                                </Col>
                              </Row>
                              <div className="text-center mt-3">
                                <Button className="bg-red border-0" type="button" onClick={this.toggleModal}>
                                  Update
                                </Button>
                              </div>
                            </Form>
                            {/* confirmation modalbox */}
                            <Modal className=""  isOpen={this.state.isOpen}>
                              <ModalHeader className="bg-light-gray" toggle={this.toggleModal}>
                                <h3 className="mb-0 text-16 text-center">
                                  Confirm Details
                                </h3>
                              </ModalHeader>
                              <ModalBody className="pt-0 text-center">
                                <h5 className="text-gray mb-2 mt-4">Are You Sure To Update This Details?</h5>
                                 <Row>
                                  <Col className="text-center mt-4 pt-1" lg="12">
                                    <Button onClick={this.updateDetails} className="bg-red border-0" type="button">
                                      Confirm
                                    </Button>
                                    <Button className="bg-light-gray border-0" type="button">
                                      Cancel
                                    </Button>
                                  </Col>
                              </Row> 
                              </ModalBody>
                            </Modal>
                          </div>
                          {/* <Col className="col-auto">
                            <div className="icon icon-shape bg-warning text-white rounded-circle shadow">
                              <i className="fas fa-plus" />
                            </div>
                          </Col> */}
                        </Row>
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
              </div>
           </Container>
        </div>
      {/* Page content */}
        <Container className="mt--7" fluid>
          <Row className="">
            <Col className="mb-5 mb-xl-0" xl="12">
              <Card className="shadow">
                <CardHeader className="border-0">
                  <Row className="align-items-center">
                    <div className="col">
                      <h3 className="mb-0">Fill the Details for Reimbursement</h3>
                    </div>
                  </Row>
                </CardHeader>
                <CardBody>
                    <Form>
                      <Row>
                      <Col className="" lg="4">
                          <FormGroup>
                            <Label for="expenseSelect">Nature of Expense</Label>
                            <Input type="select" name="select" onChange={this.handleExpenseDropdownChange} id="expenseSelect">
                              <option placeholder=""> Select </option>
                              <option placeholder="Business Promotion">Business Promotion</option>
                              <option placeholder="Cellular Phone">Cellular Phone</option>
                              <option placeholder="Internet / Data Card">Internet / Data Card</option>
                              <option placeholder="Conveyance">Conveyance</option>
                              <option placeholder="Delivery">Delivery</option>
                              <option placeholder="Water / Tea / Coffee">Water / Tea / Coffee</option>
                              <option placeholder="Festival">Festival</option>
                              <option placeholder="Petty Cash">Petty Cash</option>
                              <option placeholder="Printing Stationery">Printing Stationery</option>
                              <option placeholder="Postage Courier">Postage Courier</option>
                              <option placeholder="Repair Maintenance - Computers">Repair  Maintenance - Computers</option>
                              <option placeholder="Repair Maintenance - Vehicles">Repair  Maintenance - Vehicles</option>
                              <option placeholder="Repair Maintenance - Building">Repair  Maintenance - Building</option>
                              <option placeholder="Repair Maintenance - Others">Repair  Maintenance - Others</option>
                              <option placeholder="Staff Welfare - Buzz Night">Staff Welfare - Buzz Night</option>
                              <option placeholder="Staff Welfare - In Office">Staff Welfare - In Office</option>
                              <option placeholder="Tea Meal">Tea Meal</option>
                              <option placeholder="Telephone">Telephone</option>
                              <option placeholder="Travelling - Domestic Others">Travelling - Domestic Others</option>
                              <option placeholder="Travelling - Foreign Others">Travelling - Foreign Others</option>
                              <option placeholder="Vehicle Parking">Vehicle Parking</option>
                              <option placeholder="Vehicle Petrol - Commercial">Vehicle Petrol - Commercial</option>
                            </Input>
                          </FormGroup>
                        </Col>
                        <Col className="" lg="5">
                          <Row>
                            <Col className="" lg="6">
                              <FormGroup>
                                <Label for="FromDate">Bill Period From <span className="text-red">*</span></Label>
                                <Input
                                  type="date"
                                  name="date"
                                  id="FromDate"
                                  placeholder="date placeholder"
                                  onChange={this.handleBillPeriodFrom}
                                />
                              </FormGroup>
                            </Col>
                            <Col className="" lg="6">
                              <FormGroup>
                                <Label for="ToDate">To <span className="text-red">*</span></Label>
                                <Input
                                  type="date"
                                  name="date"
                                  id="ToDate"
                                  placeholder="date placeholder"
                                  onChange={this.handleBillPeriodTo}
                                />
                              </FormGroup>
                            </Col>
                          </Row>
                        </Col>
                        <Col className="" lg="3">
                            <FormGroup>
                              <Label for="amount">Amount <span className="text-red">*</span></Label>
                              <Input
                                type="number"
                                name="amount"
                                placeholder="Amount"
                                onChange={this.handleAmount}
                              />
                            </FormGroup>
                          </Col>
                          <Col lg="12">
                            <FormGroup>
                              <Label for="description">Expense Description <span className="text-red">*</span></Label>
                              <Input type="textarea" name="text" id="description" />
                            </FormGroup>
                          </Col>
                          <Col lg="12">
                          <h3>Request Type <span className="text-red">*</span> <small>(Online mode is mandatory now)</small>
                          </h3>
                          </Col>
                          <Col lg="6">
                           <FormGroup>
                              <Label for="description">Scanned Bills <span className="text-red">*</span></Label>
                              <Input type="file" name="file"  multiple onChange={this.onFileChangeHandler} id="Upload" />
                              <FormText color="red">
                                  Note:- Please attach in .pdf, .xls(x), .doc format only
                              </FormText>
                            </FormGroup>
                          </Col>
                          <Col lg="6">
                           <FormGroup>
                              <Label for="description">Expense detail<span className="text-red">*</span></Label>
                              <Input type="file"  multiple onChange={this.onFileChangeHandler2} name="file" id="Upload" />
                              <FormText color="red">
                                Note:- Please attach in .xls(x) format only
                              </FormText>
                            </FormGroup>
                          </Col>
                      </Row>
                    </Form>
                </CardBody>
                <div className="text-center mt-3 mb-3">
                  <Button className="bg-red border-0" type="button" onClick={this.applyExpence}>
                    Submit
                  </Button>
                </div>
                {/* Thanks modalbox */}
                <Modal className=""  isOpen={this.state.isActive}>
                  <ModalHeader className="bg-light-gray" toggle={this.toggleThanksModal}>
                    <h3 className="mb-0 text-16">
                      Thanks For Using Expense Claim Tool
                    </h3>
                  </ModalHeader>
                  <ModalBody className="pt-0 text-center">
                    <div className="icon icon-shape bg-success text-white rounded-circle shadow mb-4 mt-3">
                        <i className="ni ni-check-bold" />
                    </div>
                        <h5 className="text-gray mb-4">Your Unique Refrence ID is <b className="text-success text-16">{this.state.uniqueCode}</b>. Voucher Submited is Offline. To check Status <Link href="#" className="text-blue" to="admin/payment-traker-report">Click Here</Link></h5>
                  
                  </ModalBody>
                </Modal>
              </Card>
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

export default ReimbursementForm;
