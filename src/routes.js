//Auth Routes
import Login from "views/auth-page/Login.js";
import Forget from "views/auth-page/Forget.js";

//common pages
import UserProfile from "views/common/UserProfile.jsx";

// Stationery Routes
import StationeryMain from "views/stationery/Main.jsx";
import Productreport from "views/stationery/Productreport.jsx";
import AddProduct from "views/stationery/admin-pages/AddProduct.jsx"
import Adminreport from "views/stationery/admin-pages/Adminreport.jsx"

// Expense Claim Routes
import ReimbursementForm from "views/expense-claim/ReimbursementForm.jsx";
import ReimbursementTrackerReport from "views/expense-claim/ReimbursementTrackerReport.jsx";

// Expense Claim Approver Routes
import Approver from "views/expense-claim/admin/Approver.jsx";
import ApproverPrint from "views/expense-claim/admin/ApproverPrint.jsx";

// Expense Claim Incentive Routes
import Incentive from "views/expense-claim/incentive/Incentive.jsx";

// Expense Claim Imprest Routes
import Imprest from "views/expense-claim/imprest/Imprest.jsx";

// Expense Claim Supplier Routes
import SupplierForm from "views/expense-claim/supplier/SupplierForm.jsx";
import SupplierTrackerReport from "views/expense-claim/supplier/SupplierTrackerReport.jsx";


//Other Routes
import Index from "views/Index.js";
import VendorForm from "views/expense-claim/vendor/VendorForm";
import VendorTrackerReport from "views/expense-claim/vendor/VendorTrackerReport";

var routes = [
  //Auth Routes
  {
    path: "/login",
    name: "Login",
    icon: "ni ni-key-25",
    component: Login,
    layout: "/auth",
    children:null
  },
  {
    path: "/forget",
    name: "Forget",
    icon: "ni ni-key-25",
    component: Forget,
    layout: "/auth",
    children:null
  },

  //common page menu
  {
    path: "/user-profile",
    name: "User Profile",
    icon: "ni ni-single-02",
    component: UserProfile,
    layout: "/admin",
    children:null
  },

  // Stationery Routes
  {
    path: "/main",
    name: "Dashboard",
    icon: "ni ni-tv-2",
    component: StationeryMain,
    layout: "/admin",
    children:null
  },
  {
    path: "/report",
    name: "Stationery Report",
    icon: "ni ni-single-copy-04",
    component: Productreport,
    layout: "/admin",
    children:null
  },
  //Stationery ADMIN PAGE
  {
    path: "/addproduct",
    name: "Stationery Add Product",
    icon: "ni ni-fat-add",
    component: AddProduct,
    layout: "/admin",
    children:null
  },
  {
    path: "/adminreport",
    name: "Product Status Report",
    icon: "ni ni-fat-add",
    component: Adminreport,
    layout: "/admin",
    children:null
  },

  // Expense Routes
  {
    path: "/reimbursement-form",
    name: "Reimbursement Form",
    icon: "ni ni-tv-2",
    component: ReimbursementForm,
    layout: "/admin",
    children:null
  },
  {
    path: "/payment-traker-report",
    name: "Reimbursement Payment Tracker Report",
    icon: "ni ni-tv-2",
    component: ReimbursementTrackerReport,
    layout: "/admin",
    children:null
  },
  // Reimbursement Approver Router
  {
    path: "/approver",
    name: "Reimbursement Payment Approver",
    icon: "ni ni-tv-2",
    component: Approver,
    layout: "/admin",
    children:null
  },

  {
    path: "/print",
    name: "Approver Print Preview",
    icon: "ni ni-tv-2",
    component: ApproverPrint,
    layout: "/admin",
  },
  // Incentive Router
  {
    path: "/incentive",
    name: "Incentive Report",
    icon: "ni ni-tv-2",
    component: Incentive,
    layout: "/admin",
    children:null
  },
  // Imprest Router
  {
    path: "/imprest",
    name: "Imprest Report",
    icon: "ni ni-tv-2",
    component: Imprest,
    layout: "/admin",
    children:null
  },
  // Supplier Router
  {
    path: "/supplier-form",
    name: "Supplier Form",
    icon: "ni ni-tv-2",
    component: SupplierForm,
    layout: "/admin",
    children:null
    
  },
  {
    path: "/supplier-report",
    name: "Supplier Tracker Report",
    icon: "ni ni-tv-2",
    component: SupplierTrackerReport,
    layout: "/admin",
    children:null
  },


   // Vendor Router
   {
    path: "/vendor-form",
    name: "Vendor Form",
    icon: "ni ni-tv-2",
    component: VendorForm,
    layout: "/admin",
    children:null
    
  },
  {
    path: "/vendor-report",
    name: "Vendor Tracker Report",
    icon: "ni ni-tv-2",
    component: VendorTrackerReport,
    layout: "/admin",
    children:null
  },


  // Other Routes
  {
    path: "/index",
    name: "Dashboard",
    icon: "ni ni-tv-2",
    component: SupplierTrackerReport,
    layout: "/admin",
    children: [
      {
        path: "/supplier-report",
        name: "Dashboard",
        icon: "ni ni-tv-2",
        component: Index,
        layout: "/admin",
      },
    ]
  }
  
];
export default routes;
